#include <iostream>
#include <ctime>
#include <cstdlib>

int cnt = 20;
int range = 10;

using namespace std;

int main(const int argc, const char* argv[])
{
	if (argc == 3)
	{
		cnt = atoi(argv[1]);
		range = atoi(argv[2]);
	}
	else if(argc == 2)
	{
		cnt = atoi(argv[1]);
	}
	time_t timer;
	srand(time(&timer));
	for (int i = 0; i < (cnt - 1); ++i)
	{
		cout << rand() % range << ", ";
	}
	cout << rand() % range << endl;
	return 0;
}
