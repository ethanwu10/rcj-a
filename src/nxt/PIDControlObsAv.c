#include "Headers\obstacleAvoidance.h"

const tSensors US = S4;
const tMotor LM = motorB;
const tMotor RM = motorC;

const string side = "left";

int checkVarSide(const string sideStr)
{
	int sideNum = side == "right" ? 1 : -1;
	return sideNum;
}

task main()
{
	int sidePID = checkVarSide(side);
	while(nNxtButtonPressed != kEnterButton)
	{
		PIDControl(bPower, tDist, pGain, dGain, iGain, sidePID);
	}
	StopAllTasks();
}
