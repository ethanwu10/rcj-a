#include "Headers\MSLSA.h"

MSLSA lightArray = {S1};

#define RAW
#define PERCENT

task main()
{
	Init_MSLSA(lightArray);

	nNxtExitClicks = 2;
	while (nNxtButtonPressed != kExitButton)
	{
		Update_MSLSA(lightArray);

#ifdef RAW
		/***************RAW**********************************************/
		for (int i = 0; i < 8; i++)
		{
		nxtDisplayTextLine(i, "S[%d]: %d", i, lightArray.raw[i]);
		}
		/****************************************************************/
#endif //RAW

#ifdef PERCENT
		/***************PERCENT******************************************/
		nxtEraseRect(6,62, 91, 43);
		for (int i = 0; i < 8; i++)
		{
			nxtDrawRect(6+(i*11),62, 14+(i*11), 50);
			nxtFillRect(6+(i*11),51+lightArray.percent[i]/10, 14+(i*11), 50);
		}
		for (int i = 0; i < 7; i+=2)
		{
			nxtDisplayTextLine(i/2+3, "S%d: %3d S%d: %3d", i, (int)lightArray.percent[i], i + 1, (int)lightArray.percent[i+1]);
		}
		/****************************************************************/
		Sleep(50);
#endif //PERCENT

	}
	Sleep_MSLSA(lightArray);
}
