#include "Headers/MPU6050.h"

task main()
{
	SensorType[MPU] = sensorI2CCustom;
	MPU6050_deviceReset(MPU);
	MPU6050_wakeUp(MPU);

	int gyroX, gyroY, gyroZ;
	int accelX, accelY, accelZ;
	while (true)
	{
		MPU6050_readGyroX(MPU, gyroX);
		MPU6050_readGyroY(MPU, gyroY);
		MPU6050_readGyroZ(MPU, gyroZ);
		MPU6050_readAccelX(MPU, accelX);
		MPU6050_readAccelY(MPU, accelY);
		MPU6050_readAccelZ(MPU, accelZ);
		nxtDisplayTextLine(1, "Gyro X:  % 6d", gyroX);
		nxtDisplayTextLine(2, "Gyro Y:  % 6d", gyroY);
		nxtDisplayTextLine(3, "Gyro Z:  % 6d", gyroZ);
		nxtDisplayTextLine(4, "Accel X: % 6d", accelX);
		nxtDisplayTextLine(5, "Accel Y: % 6d", accelY);
		nxtDisplayTextLine(6, "Accel Z: % 6d", accelZ);
	}
}
