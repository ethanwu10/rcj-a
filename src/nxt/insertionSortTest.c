#include "Headers\insertionSort.h"

const int num_elements = 10;

task main()
{
	unsigned int time;
	int num[num_elements];
	int numOrig[num_elements];
	for (int i = 0; i < num_elements; ++i)
	{
		num[i] = abs(rand()) % num_elements; //Generate random numbers
	}
	memmove(numOrig, num, sizeof(num));
	ClearTimer(T1);
	sort(num, num_elements);
	time = time1[T1];
	nxtDisplayCenteredTextLine(0, "Time: %d ms", time);
	for(int i = 0; i < (num_elements / 2); ++i)
	{
		nxtDisplayCenteredTextLine(i + 1, "%d\t%d", num[i], num[i+5]);
	}
	while (nNxtButtonPressed != kEnterButton)
	{
		continue;
	}
}
