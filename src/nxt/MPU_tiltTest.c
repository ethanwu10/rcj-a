#include "Headers\MPU6050.h"
#include "Headers\mathFunc.h"
#include "Headers\vehicle.h"

#define sqr(x) pow(x,2)

#define rad2deg(rad) ((rad)/(float)PI*180)

int XOffs = -378;
int YOffs = -3264;
int ZOffs = 1711;


task main()
{
	SensorType[MPU] = sensorI2CCustom;
	Sleep(500);
	if (!MPU6050_testConnection(MPU))
	{
		PlaySound(soundException);
		nxtDisplayCenteredTextLine(4, "Comm failed");
		Sleep(1000);
		StopAllTasks();
	}
	//MPU6050_deviceReset(MPU);
	MPU6050_wakeUp(MPU);

	{
		MPU6050_I2CRequest[0] = 4;
		MPU6050_I2CRequest[1] = MPU6050_NXT_ADDRESS;
		MPU6050_I2CRequest[2] = MPU6050_RA_XA_OFFS_H;
		MPU6050_I2CRequest[3] = (XOffs >> 8);   //MSB
		MPU6050_I2CRequest[4] = (XOffs & 0xFF); //LSB
		writeI2C(MPU, MPU6050_I2CRequest);
		//writeDebugStreamLine("%02x%02x", MPU6050_I2CRequest[3], MPU6050_I2CRequest[4]);

		MPU6050_I2CRequest[0] = 4;
		MPU6050_I2CRequest[1] = MPU6050_NXT_ADDRESS;
		MPU6050_I2CRequest[2] = MPU6050_RA_YA_OFFS_H;
		MPU6050_I2CRequest[3] = (YOffs >> 8);   //MSB
		MPU6050_I2CRequest[4] = (YOffs & 0xFF); //LSB
		writeI2C(MPU, MPU6050_I2CRequest);
		//writeDebugStreamLine("%02x%02x", MPU6050_I2CRequest[3], MPU6050_I2CRequest[4]);

		MPU6050_I2CRequest[0] = 4;
		MPU6050_I2CRequest[1] = MPU6050_NXT_ADDRESS;
		MPU6050_I2CRequest[2] = MPU6050_RA_ZA_OFFS_H;
		MPU6050_I2CRequest[3] = (ZOffs >> 8);   //MSB
		MPU6050_I2CRequest[4] = (ZOffs & 0xFF); //LSB
		writeI2C(MPU, MPU6050_I2CRequest);
		//writeDebugStreamLine("%02x%02x", MPU6050_I2CRequest[3], MPU6050_I2CRequest[4]);
	}

	int accelX, accelY, accelZ;
	while (true)
	{
		MPU6050_readAccelX(MPU, accelX);
		MPU6050_readAccelY(MPU, accelY);
		MPU6050_readAccelZ(MPU, accelZ);

		nxtDisplayTextLine(0, "Accel X:  % 6d", accelX);
		nxtDisplayTextLine(1, "Accel Y:  % 6d", accelY);
		nxtDisplayTextLine(2, "Accel Z:  % 6d", accelZ);

		float G;
		int AngleTilt, AngleRotate;
		{
			float gX, gY, gZ;
			gX = accelX / 16384.0;
			gY = accelY / 16384.0;
			gZ = accelZ / 16384.0;
			G = sqrt(sqr(gX) + sqr(gY) + sqr(gZ));
			AngleRotate = radiansToDegrees(atan((float)gX / (float)gY));
			AngleTilt = radiansToDegrees(atan(sqrt(sqr(gX) + sqr(gY)) / gZ));

			AngleTilt *= getSign(accelY);
			if (AngleRotate > 180)
			{
				AngleRotate -= 360;
			}
		}
		nxtDisplayTextLine(4, "%.3fG", G);
		nxtDisplayTextLine(5, "Tilt:     % 4d�", AngleTilt);
		nxtDisplayTextLine(6, "Rotation: % 4d�", AngleRotate);
	}
}
