#include "Headers\obstacleAvoidance.h"

const tSensors US = S4;

const int bPower_test = 20;
const float tDist_test = 20.0;
const float pGain_test = 1.0;
const float dGain_test = 1.0;
const float iGain_test = 0.0;
const string side = "left";

int checkVarSide(const string sideStr)
{
int sideNum = side == "right" ? 1 : -1;
	return sideNum;
}

task main()
{
	int sidePID = checkVarSide(side);
	while(nNxtButtonPressed != kEnterButton)
	{
		PIDControl(bPower_test, tDist_test, pGain_test, dGain_test, iGain_test, sidePID, true);
	}
	StopAllTasks();
}
