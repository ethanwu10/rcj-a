#include "Headers\vehicle.h"                     //constants
#include "Headers\MSLSALineFollow_turn.h"        //line following
#include "Headers\obstacleAvoidance_noPID.h"     //obstacle avoidance
#include "Headers\ramp.h"                        //ramp and ramp detection
#include "Headers\evacRoom.h"                    //evacuation room
#include "Headers\arduinoIR.h"                   //silver line detection

#include "Headers\MPU6050.h"                     //accel lib


#define ACCELRAMP


typedef enum
{
	actCode_obsAvoid,
	actCode_ramp,
	actCode_evacRoom
} TactCode;

task main()
{
	SensorType[Arduino] = sensorI2CCustom9V;
	//SensorType[silverIR] = sensorTouch;
	SensorType[MPU] = sensorI2CCustom9V;
	MPU6050_deviceReset(MPU);
	arduinoPing_setConf(Arduino, 7);
	while(true)
	{
		PIDCorrect();
		StartTask(lineFollow);
		Sleep(500);
		{
			ubyte ping_pwr_conf = arduinoPing_getConf(Arduino); //retain sensors enabled by other tasks/programs
			setBit(ping_pwr_conf, SensorFront);
			arduinoPing_setConf(Arduino, ping_pwr_conf);
		}
		Sleep(500);
		TactCode actCode;
		while(true)
		{
			if (LineFollowState != LineFollowState_Gap && arduinoPing_readInt(Arduino, SensorFront) < tDist)
			{
				actCode = actCode_obsAvoid;
				break;
			}
			if (/*LineFollowState == LineFollowState_Gap &&*/ arduinoIR_readValue(Arduino))
			{
				actCode = actCode_evacRoom;
				break;
			}
#ifdef ACCELRAMP
			if (isRamp())
			{
				actCode = actCode_ramp;
				break;
			}
#endif //ACCELRAMP
			Sleep(100); //line tracing
		}
		if (actCode == actCode_obsAvoid)
		{
			StopTask(lineFollow);
			motor[motorLeft] = 0;
			motor[motorRight] = 0;
			obstacleAvoidance();
			motor[motorLeft] = 0;
			motor[motorRight] = 0;
			Sleep(50);
			continue;
		}
		if (actCode == actCode_ramp)
		{
			StopTask(lineFollow);
			motor[motorLeft] = 0;
			motor[motorRight] = 0;
			ramp();
			motor[motorLeft] = 0;
			motor[motorRight] = 0;
			Sleep(100);
			continue;
		}
		if (actCode == actCode_evacRoom)
		{
			StopTask(lineFollow);
			motor[motorLeft] = 0;
			motor[motorRight] = 0;
			evacRoom();
			break;
		}
	}

}
