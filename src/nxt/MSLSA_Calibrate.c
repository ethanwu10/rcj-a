#include "Headers\MSLSA.h"

MSLSA lightArray = {S1};

#define CALIBRATE

#define RAW
#define PERCENT

task main()
{
	Init_MSLSA(lightArray);

#ifdef CALIBRATE
	nxtDisplayCenteredTextLine(3, "Press <ENTER> to");
	nxtDisplayCenteredTextLine(4, "calibrate black");
	while (nNxtButtonPressed != kEnterButton)
	{
		continue;
	}
	BlackCal_MSLSA(lightArray);
	while (nNxtButtonPressed == kEnterButton) //Debounce
	{
		continue;
	}
	nxtDisplayCenteredTextLine(3, "Press <ENTER> to");
	nxtDisplayCenteredTextLine(4, "calibrate white");
	while (nNxtButtonPressed != kEnterButton)
	{
		continue;
	}
	WhiteCal_MSLSA(lightArray);
	while (nNxtButtonPressed == kEnterButton) //Debounce
	{
		continue;
	}
#endif //CALIBRATE

	nNxtExitClicks = 2;
	while (nNxtButtonPressed != kExitButton)
	{
		Update_MSLSA(lightArray);

#ifdef RAW
		/***************RAW**********************************************/
		for (int i = 0; i < 8; i++)
		{
		nxtDisplayTextLine(i, "S[%d]: %d", i, lightArray.raw[i]);
		}
		/****************************************************************/
#endif //RAW

#ifdef PERCENT
		/***************PERCENT******************************************/
		nxtEraseRect(6,62, 91, 43);
		for (int i = 0; i < 8; i++)
		{
			nxtDrawRect(6+(i*11),62, 14+(i*11), 50);
			nxtFillRect(6+(i*11),51+lightArray.percent[i]/10, 14+(i*11), 50);
		}
		for (int i = 0; i < 7; i+=2)
		{
			nxtDisplayTextLine(i/2+3, "S: %3d S: %3d", (int)lightArray.percent[i], (int)lightArray.percent[i+1]);
		}
		/****************************************************************/
		Sleep(50);
#endif //PERCENT

	}
	Sleep_MSLSA(lightArray);
}
