#include "joystickDriver.c"


#define DEBUG


const tMotor motorLeft = motorC;
const tMotor motorRight = motorB;

const float defaultScale = 1.28;

unsigned short maxPower = 75;
float scale = 1.28;

task updateJoy()
{
	while (true)
	{
		getJoystickSettings(joystick);
	}
}

task updatePower()
{
	while (true)
	{
		if (joystick.joy1_Buttons & 0x10)
		{
			if (maxPower > 0)
			{
				maxPower -= 1;
			}
		}
		if (joystick.joy1_Buttons & 0x20)
		{
			if (maxPower < 100)
			{
				maxPower += 1;
			}
		}
		if (joystick.joy1_Buttons & 0x30)
		{
			scale = (defaultScale / (maxPower / 100.0));
		}
		Sleep(50);
#ifdef DEBUG
		nxtDisplayCenteredTextLine(3, "%d%%", maxPower);
#endif //DEBUG
	}
}

task joyDrive()
{
	while (true)
	{
		motor[motorRight] = (joystick.joy1_y2 / scale);
		motor[motorLeft] = (joystick.joy1_y1 / scale);
	}
}

task DDrive()
{
	while (true)
	{
		switch (joystick.joy1_TopHat)
		{
		case 0:
			motor[motorRight] = maxPower;
			motor[motorLeft] = maxPower;
			break;
		case 1:
			motor[motorRight] = 0;
			motor[motorLeft] = maxPower;
			break;
		case 2:
			motor[motorRight] = (-1 * maxPower);
			motor[motorLeft] = maxPower;
			break;
		case 3:
			motor[motorRight] = 0;
			motor[motorLeft] = (-1 * maxPower);
			break;
		case 4:
			motor[motorRight] = (-1 * maxPower);
			motor[motorLeft] = (-1 * maxPower);
			break;
		case 5:
			motor[motorRight] = (-1 * maxPower);
			motor[motorLeft] = 0;
			break;
		case 6:
			motor[motorRight] = maxPower;
			motor[motorLeft] = (-1 * maxPower);
			break;
		case 7:
			motor[motorRight] = maxPower;
			motor[motorLeft] = 0;
			break;
		default:
		}
	}
}

task driveCtrlDPriority()
{
	bool DPadPressed;
	StartTask(joyDrive);
	while (true)
	{
		if (joystick.joy1_TopHat == -1)
		{
			if (DPadPressed)
			{
				StopTask(DDrive);
				StartTask(joyDrive);
			}
			DPadPressed = false;
		}
		else
		{
			if (!(DPadPressed))
			{
				StopTask(joyDrive);
				StartTask(DDrive);
			}
			DPadPressed = true;
		}
	}
}

task driveCtrlJoyPriority()
{
}

task main()
{
	StartTask(updateJoy);
	StartTask(updatePower);
	while (!(joystick.joy1_Buttons & 0x200))
	{
		continue;
	}
	StartTask(driveCtrlDPriority);
	while (true)
	{
		continue;
	}
}
