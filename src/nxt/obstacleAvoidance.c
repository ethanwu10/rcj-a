#include "Headers\MSLSALineFollow.h"
#include "Headers\obstacleAvoidance.h"

task main()
{
	SensorType[US] = sensorSONAR;
	PIDCorrect();
	StartTask(lineFollow);
	while(!(SensorValue[US] < tDist))
	{
		//line tracing
	}
	StopTask(lineFollow);
	motor[LM] = 0;
	motor[RM] = 0;
	StartTask(obstacleAvoidance);
	while(!obsAvoid_done)
	{
		continue;
	}
}
