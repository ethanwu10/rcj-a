#pragma once

#pragma systemFile


#include "arduino.h"
#include "bitmap.h"


#define ARDUINO_REG_PING_CONF         0x20
#define ARDUINO_REG_PING_MODE         0x21
#define ARDUINO_REG_PING_MODE_CFG     0x22
#define ARDUINO_REG_PING_INTEGER_BASE 0x30
#define ARDUINO_REG_PING_DECIMAL_BASE 0x40

#define ARDUINO_PING_MODE_MEDIAN 0
#define ARDUINO_PING_MODE_FAST   1


#ifdef JOSEPH
	#define SensorFront 2
	#define SensorLeft 0
	#define SensorRight 1
#endif
#ifdef ETHAN
	#define SensorFront 2
	#define SensorLeft 0
	#define SensorRight 1
#endif

//PROTOTYPES////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ubyte arduinoPing_readInt(tSensors port, ubyte SNum);                                         //read int values from given sensor
float arduinoPing_readFloat(tSensors port, ubyte SNum);                                       //read float values from given sensor

void arduinoPing_setConf(tSensors port, ubyte config);                                        //set sensor config (with bitmask value)
void arduinoPing_setConf(tSensors port, bool S0_active, bool S1_active, bool S2_active);      //set sensor config (with bool flag for each)
ubyte arduinoPing_getConf(tSensors port);                                                     //get sensor config (with bitmask value)
void arduinoPing_setMode(tSensors port, ubyte mode);                                          //set sensor mode
ubyte arduinoPing_getMode(tSensors port);                                                     //get sensor mode
void arduinoPing_setModeCfg(tSensors port, ubyte cfg);                                        //set sensor mode config
ubyte arduinoPing_getModeCfg(tSensors port);                                                  //get sensor mode config


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


ubyte _arduinoPing_GetIntegerReg(ubyte SNum)
{
	if (SNum > 0xf)
	{
		return 0; //TODO: better error handling
	}
	return SNum | ARDUINO_REG_PING_INTEGER_BASE;
}

ubyte _arduinoPing_GetDecimalReg(ubyte SNum)
{
	if (SNum > 0xf)
	{
		return 0; //TODO: better error handling
	}
	return SNum | ARDUINO_REG_PING_DECIMAL_BASE;
}


void arduinoPing_setConf(tSensors port, ubyte config)
{
	writeArduinoReg_byte(port, ARDUINO_REG_PING_CONF, config);
}

void arduinoPing_setConf(tSensors port, bool S0_active, bool S1_active, bool S2_active)
{
	ubyte config = 0;
	writeBit(config, 0, S0_active);
	writeBit(config, 1, S1_active);
	writeBit(config, 2, S2_active);
	arduinoPing_setConf(port, config);
}

ubyte arduinoPing_getConf(tSensors port)
{
	return readArduinoReg_byte(port, ARDUINO_REG_PING_CONF);
}

void arduinoPing_setMode(tSensors port, ubyte mode)
{
	writeArduinoReg_byte(port, ARDUINO_REG_PING_MODE, mode);
}

ubyte arduinoPing_getMode(tSensors port)
{
	return readArduinoReg_byte(port, ARDUINO_REG_PING_MODE);
}

void arduinoPing_setModeCfg(tSensors port, ubyte cfg)
{
	writeArduinoReg_byte(port, ARDUINO_REG_PING_MODE_CFG, cfg);
}

ubyte arduinoPing_getModeCfg(tSensors port)
{
	return readArduinoReg_byte(port, ARDUINO_REG_PING_MODE_CFG);
}

ubyte arduinoPing_readInt(tSensors port, ubyte SNum)
{
	if (SNum > 0xf)
	{
		return 0; //TODO: better error handling
	}
	return readArduinoReg_byte(port, _arduinoPing_GetIntegerReg(SNum));
}

float arduinoPing_readFloat(tSensors port, ubyte SNum)
{
	if (SNum > 0xf)
	{
		return 0; //TODO: better error handling
	}
	ubyte valInt = arduinoPing_readInt(port, SNum);
	ubyte valDecPoints = readArduinoReg_byte(port, _arduinoPing_GetDecimalReg(SNum));
	return valInt + (valDecPoints / 100.0);
}
