#ifndef BLACKDETECT_H
#define BLACKDETECT_H

#include "MSLSA.h"

bool isBlackLine(MSLSA link, int blackThresh)
{
	Update_MSLSA(link);
	bool isBlack = false;
	for (int i = 0; i < 8; ++i)
	{
		if (link.percent[i] < blackThresh)
		{
			isBlack = true;
			break;
		}
	}
	return isBlack;
}

bool isBlackLine(tSensors link, int blackThresh)
{
	MSLSA newLink;
	newLink.port = link;
	return isBlackLine(newLink, blackThresh);
}

#endif //BLACKDETECT_H