#pragma once

#include "vehicle.h"
#include "PID.h"
#include "arduinoPing.h"
#include "bitmap.h"
#include "mathFunc.h"

const float PGain = 1;
const float IGain = 0;
const float DGain = 2;

//const int tDist = 20;




const float hallWidth = 21.5;


task rampWF()
{
	SensorType[Arduino] = sensorI2CCustom;
	{
		ubyte ping_pwr_conf = 0;
		setBit(ping_pwr_conf, SensorLeft);
		setBit(ping_pwr_conf, SensorRight);
		arduinoPing_setConf(Arduino, ping_pwr_conf);
		arduinoPing_setMode(Arduino, ARDUINO_PING_MODE_FAST);
	}
	int rampWFBasePower = 75;
	float error = 0;
	int correction = 0;
	motor[motorLeft]  = 20;
	motor[motorRight] = 20;

	while(true)
	{
		float left, right;
		left  = arduinoPing_readFloat(Arduino, SensorLeft);
		right = arduinoPing_readFloat(Arduino, SensorRight);
		error = (left - right) * abs(left + right - hallWidth + 5.0);
		correction = PIDCorrect(error, PGain, IGain, DGain);
		if (abs(correction) < 150)
			break;
	}
	PlaySound(soundBeepBeep);
	while(true)
	{
		float left, right;
		left  = arduinoPing_readFloat(Arduino, SensorLeft);
		right = arduinoPing_readFloat(Arduino, SensorRight);
		error = (left - right) * abs(left + right - hallWidth + 5.0);
		correction = PIDCorrect(error, PGain, IGain, DGain);
		if ((left + right) > 30)
			break;
		motor[motorRight] = max(rampWFBasePower + correction, 0);
		motor[motorLeft]  = max(rampWFBasePower - correction, 0);
		nxtDisplayTextLine(2,"rampWFBasePower + correction");
		nxtDisplayTextLine(3,"rampWFBasePower - correction");
		Sleep(5);
	}
	motor[motorLeft]  = 20;
	motor[motorRight] = 20;
}
