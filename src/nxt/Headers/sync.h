#ifndef SYNC_H
#define SYNC_H

#pragma systemFile

typedef struct sync
{
	tMotor motorMaster;
	tMotor motorSlave;
} sync;

bool validateSync(sync syncMode)
{
	return syncMode.motorMaster != syncMode.motorSlave;
}

TSynchedMotors getSyncMode(sync syncMode)
{
	switch (syncMode.motorMaster)
	{
	case motorA:
		switch (syncMode.motorSlave)
		{
		case motorB:
			return synchAB;
		case motorC:
			return synchAC;
		default:
			return synchNone;
		}
	case motorB:
		switch (syncMode.motorSlave)
		{
		case motorA:
			return synchBA;
		case motorC:
			return synchBC;
		default:
			return synchNone;
		}
	case motorC:
		switch (syncMode.motorSlave)
		{
		case motorA:
			return synchCA;
		case motorB:
			return synchCB;
		default:
			return synchNone;
		}
	default:
		return synchNone;
	}
}

TSynchedMotors getSyncMode(tMotor motorMaster, tMotor motorSlave)
{
	sync syncMode;
	syncMode.motorMaster = motorMaster;
	syncMode.motorSlave = motorSlave;
	return getSyncMode(syncMode);
}

bool getMotors(sync& syncMotors, TSynchedMotors syncMode)
{
	tMotor motorMaster;
	tMotor motorSlave;
	switch (syncMode)
	{
	case synchAB:
		motorMaster = motorA;
		motorSlave = motorB;
		break;
	case synchAC:
		motorMaster = motorA;
		motorSlave = motorC;
		break;
	case synchBA:
		motorMaster = motorB;
		motorSlave = motorA;
		break;
	case synchBC:
		motorMaster = motorB;
		motorSlave = motorC;
		break;
	case synchCA:
		motorMaster = motorC;
		motorSlave = motorA;
		break;
	case synchCB:
		motorMaster = motorC;
		motorSlave = motorB;
		break;
	default:
		return false;
	}
	syncMotors.motorMaster = motorMaster;
	syncMotors.motorSlave = motorSlave;
	return true;
}

#endif
