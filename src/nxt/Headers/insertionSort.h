#ifndef INSERTIONSORT_H
#define INSERTIONSORT_H

int* sort(int* num, int sizeof_num)
{
	for (int sortNum = 1; sortNum < sizeof_num; ++sortNum)
	{
		if (num[sortNum] > num[sortNum - 1]) //Huge optimization for almost-sorted arrays
		{
			continue;
		}
		else
		{
			for (int compareNum = 0; compareNum < sortNum; ++compareNum)
			{
				if (num[compareNum] > num[sortNum])
				{
					int sortNumVal = num[sortNum];
					memmove(num + compareNum + 1, num + compareNum, sizeof(int) * (sortNum - compareNum));
					num[compareNum] = sortNumVal;
					break;
				}
			}
		}
	}
	return num;
}

#endif //INSERTIONSORT_H