#pragma once

#include "common.h"
#pragma systemFile

#define ARDUINO_ADDR 0x05

#define ARDUINO_NXT_ADDR (ARDUINO_ADDR << 1)


tByteArray msg;
tByteArray reply;

ubyte readArduinoReg_byte(tSensors port, ubyte reg)
{
	memset(msg, 0, sizeof(msg));
	msg[0] = 2;
	msg[1] = ARDUINO_NXT_ADDR;
	msg[2] = reg;
	writeI2C(port, msg, reply, 1);
	return reply[0];
}

void writeArduinoReg_byte(tSensors port, ubyte reg, ubyte val)
{
	memset(msg, 0, sizeof(msg));
	msg[0] = 3;
	msg[1] = ARDUINO_NXT_ADDR;
	msg[2] = reg;
	msg[3] = val;
	writeI2C(port, msg);
}
