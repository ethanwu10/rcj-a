#ifndef MATHFUNC_H
#define MATHFUNC_H

#pragma systemFile

#define min(x, y) (x < y ? x : y)
#define max(x, y) (x > y ? x : y)
#define clamp(x, a, b) (max(a, min(b, x)))

#define isNegative(x) ((bool)(x & (1 << (sizeof(x) * 8 - 1))))
#define getSign(x) (isNegative(x) ? -1 : 1)

#define tan(x) (sin(x) / cos(x))

#define sqr(x) pow(x, 2)
#define cube(x) pow(x, 3)

#endif //MATHFUNC_H
