#ifndef OBSTACLEAVOIDANCE_H
#define OBSTACLEAVOIDANCE_H

#include "vehicle.h"
#include "arduinoPing.h"
#include "bitmap.h"
#include "MSLSA.h"
#include "PID.h"
#include "move.h"
#include "blackDetect.h"

#define OBSAVOID_DEBUG


const ubyte MSLSA_left = 7;
const ubyte MSLSA_right = 0;

const int wallThresh = 30;
const unsigned short lineThresh = 10;
const int obstacleThresh = 15;

const int Power = 20;
const int tDist = 5;
const int obsAvoid_backupDist = -2;

const unsigned short noObstActionThresh = 5;
const unsigned short obstActionThresh = 5;
const unsigned short lineActionThresh = 2;


typedef enum {Side_left = -1, Side_right = 1} TSide;


bool checkForLine(ubyte MSLSA_side)
{
	static unsigned short blackCnt = 0;
	if (lightArray.percent[MSLSA_side] < lineThresh)
	{
		blackCnt++;
	}
	else
	{
		blackCnt = 0;
	}
	return !(blackCnt > lineActionThresh);
}

bool obsAvoid_done;

void obstacleAvoidance()
{
	SensorType[Arduino] = sensorI2CCustom9V;
	obsAvoid_done = false;
	moveStraight(-7, 20, tDia, GR, WB);
	{
		byte ping_pwr_conf = 0;
		setBit(ping_pwr_conf, SensorLeft);
		setBit(ping_pwr_conf, SensorRight);
		arduinoPing_setConf(Arduino, ping_pwr_conf);
		Sleep(500);
	}
	TSide side = arduinoPing_readInt(Arduino, SensorRight) < wallThresh ? Side_right : Side_left;
	ubyte MSLSA_side = side == Side_right ? MSLSA_right : MSLSA_left;
	ubyte PingSensor = side == Side_left ? SensorLeft : SensorRight;
	{
		nSyncedMotors = synchBC;
		nSyncedTurnRatio = 100;
		turn(90 * side, 40, tDia, GR, WB);
		motor[motorB] = Power;
		Sleep(100);
		bool pastObst = true;
		unsigned short noObstCnt = 0;
		unsigned short obstCnt = 0;
		Update_MSLSA(lightArray);
		unsigned short turns = 0;
		while(!(turns > 0) || checkForLine(MSLSA_side))
		{
			if (((arduinoPing_readInt(Arduino, PingSensor) > obstacleThresh) && pastObst))
			{
				++noObstCnt;
			}
			else
			{
				noObstCnt = 0;
			}
			if ((pastObst == false) && (arduinoPing_readInt(Arduino, PingSensor) < obstacleThresh))
			{
				++obstCnt;
			}
			else
			{
				obstCnt = 0;
			}
			if (obstCnt > obstActionThresh)
			{
				pastObst = true;
			}
			if (noObstCnt > noObstActionThresh)
			{
				noObstCnt = 0;
				//Sleep(150);
				motor[motorB] = 0;
				writeDebugStreamLine("===============================\n");
				moveStraight(obsAvoid_backupDist, 10, tDia, GR, WB);
				pivotTurn(90, 20, tDia, GR, WB, side == Side_left ? motorRight : motorLeft);
				Sleep(500);
				pastObst = !(arduinoPing_readInt(Arduino, PingSensor) > obstacleThresh);
				//moveStraight(-3, 20, tDia, GR, WB);
				motor[motorB] = Power;
				++turns;
			}
			Update_MSLSA(lightArray);

			for(int i = 0; i < 3; i++)
			{
				writeDebugStreamLine("%d", arduinoPing_readInt(Arduino, i));
			}
			writeDebugStreamLine("\n");
		}
	}
	arduinoPing_setConf(Arduino, 0b000);
	pivotTurn(60, 20, tDia, GR, WB, side == Side_right ? motorRight : motorLeft);
	{
		Sleep(50);
		moveStraight(-6, 10, tDia, GR, WB);
	}
	nSyncedMotors = synchNone;
	obsAvoid_done = true;
}

#endif //OBSTACLEAVOIDANCE_H
