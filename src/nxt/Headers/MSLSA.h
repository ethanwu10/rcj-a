#ifndef _MSLSA_H_
#define _MSLSA_H_

#include "mindsensors-lightsensorarray.h"

//Undefine to remove advanced features
#define MSLSA_advancedFeatures

typedef enum errorCode
{
	GOOD = 0,
	READ_ERR,
	WRITE_ERR
} errorCode;

typedef struct MSLSA
{
	tSensors port;
	tByteArray percent;
	tIntArray raw;
} MSLSA;

errorCode Init_MSLSA(const MSLSA& link)	//Error code:	GOOD=OK
{
	SensorType[link.port] = sensorI2CCustomFastSkipStates9V;
	return GOOD;
}

errorCode Update_MSLSA(MSLSA& link)		//Error code:	GOOD=OK	WRITE_ERR=send command failure	READ_ERR=read error
{
	MSLSAwakeUp(link.port);
	if (!(MSLSAreadRawSensors(link.port, &link.raw[0])))
	{
		return READ_ERR;
	}
	if (!(MSLSAreadSensors(link.port, &link.percent[0])))
	{
		return READ_ERR;
	}
	return GOOD;
}

#ifdef MSLSA_advancedFeatures

errorCode Sleep_MSLSA(MSLSA& link)		//Error code:	GOOD=OK	WRITE_ERR=send command failure
{
	if (!(MSLSASleep(link.port)))
	{
		return WRITE_ERR;
	}
	return GOOD;
}

errorCode WakeUp_MSLSA(MSLSA& link)		//Error code:	GOOD=OK	WRITE_ERR=send command failure
{
	if (!(MSLSAwakeUp(link.port)))
	{
		return WRITE_ERR;
	}
	return GOOD;
}

errorCode WhiteCal_MSLSA(MSLSA& link)	//Error code:	GOOD=OK	WRITE_ERR=send command failure
{
	if (!(MSLSAcalWhite(link.port)))
	{
		return WRITE_ERR;
	}
	return GOOD;
}

errorCode BlackCal_MSLSA(MSLSA& link)	//Error code:	GOOD=OK	WRITE_ERR=send command failure
{
	if (!(MSLSAcalBlack(link.port)))
	{
		return WRITE_ERR;
	}
	return GOOD;
}

int SetEU_MSLSA(MSLSA& link)			//Error code:	GOOD=OK	WRITE_ERR=send command failure
{
	if (!(MSLSAsetEU(link.port)))
	{
		return WRITE_ERR;
	}
	return GOOD;
}

int SetUni_MSLSA(MSLSA& link)			//Error code:	GOOD=OK	WRITE_ERR=send command failure
{
	if (!(MSLSAsetUni(link.port)))
	{
		return WRITE_ERR;
	}
	return GOOD;
}

int SetUS_MSLSA(MSLSA& link)			//Error code:	GOOD=OK	WRITE_ERR=send command failure
{
	if (!(MSLSAsetUS(link.port)))
	{
		return WRITE_ERR;
	}
	return GOOD;
}

#endif //MSLSA_advancedFeatures

#endif //_MSLSA_H_
