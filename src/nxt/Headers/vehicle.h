#ifndef VEHICLE_H
#define VEHICLE_H

#include "MSLSA.h"

//#define ETHAN
//#define JOSEPH

//Any robot-specific constants go in this file

#if defined(JOSEPH) && defined(ETHAN)
	#error "Multiple robots selected"
#elif  !defined(JOSEPH) && !defined(ETHAN)
	#error "No robot selected"
#else
	#if defined(ETHAN)
			const float tDia = 6.24;
			const float GR   = 1;
			const float WB   = 10.1;
			const tSensors Arduino  = S2;
			const tSensors silverIR = S3;
			const tSensors MPU      = S3;
	#elif defined(JOSEPH)
			const float tDia = 6.24;
			const float GR   = 1;
			const float WB   = 10.1;
			const tSensors Arduino  = S2;
			const tSensors silverIR = S3;
			const tSensors MPU      = S3;
	#endif
#endif

const tMotor motorRight = motorB;
const tMotor motorLeft = motorC;

MSLSA lightArray = {S1};

#endif //VEHICLE_H
