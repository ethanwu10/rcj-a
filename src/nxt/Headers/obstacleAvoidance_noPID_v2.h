#include "masterInclude.h"
#include "blackDetectObsAvoid.h"

const int ObsDist = 7;
const int tDist = ObsDist;
const int nBlack = 20;
const int interv = 4;
const float Buffer = WB - 5.0;

typedef enumWord enSide
{
	isLeft=-1,
	isRight=1
}enSide;

typedef enumWord enDistRead
{
	cur = 0, //default is 0
	prev = 1 //default is 1
}enDistRead;

enSide checkSide(tSensors ping)
{
	int checks = arduinoPing_readInt(Arduino, SensorLeft) - arduinoPing_readInt(Arduino, SensorRight);
	bool side = checks < 0;
	return side ? isRight : isLeft;
}

float calObstDiam(enSide side, int *turnDeg)
{
	{
		ubyte ping_pwr_conf = 0;
		setBit(ping_pwr_conf, SensorFront);
		arduinoPing_setConf(Arduino, ping_pwr_conf);
		//arduinoPing_setMode(Arduino, ARDUINO_PING_MODE_FAST);
	}
	int xDist = arduinoPing_readFloat(Arduino, SensorFront);
	motorStop();
	wait1Msec(5);
	int prevDist[2] = {0, 0};
	prevDist[cur] = arduinoPing_readInt(Arduino, SensorFront);
	prevDist[prev] = prevDist[cur];
	while((prevDist[cur] - prevDist[prev]) < 3)
	{
		if(side == isLeft)
			turnLeft(interv, 10, tDia, GR, WB);
		else
			turnRight(interv, 10, tDia, GR, WB);
		motorStop();
		wait1Msec(5);
		prevDist[prev] = prevDist[cur];
		prevDist[cur] = arduinoPing_readInt(Arduino, SensorFront);
		*turnDeg += interv;
		nxtDisplayCenteredTextLine(3, " %d", prevDist[cur]);
		nxtDisplayCenteredTextLine(4, "-%d", prevDist[prev]);
	}
	float sinTheta = sin(degreesToRadians(*turnDeg));
	nxtDisplayCenteredTextLine(1, "Degrees: %d", *turnDeg);
	float obsRadius = (xDist * sinTheta)/(1.0 - sinTheta);
	return obsRadius;
}

void goArd(float rad, enSide side, int turnDeg = 0)
{
	float rIn = 2 * (rad + Buffer);
	float rOut = rIn + 2 * WB;
	float ratMot = rIn / rOut;
	eraseDisplay();
	nxtDisplayCenteredTextLine(3, "%.2f, %c", ratMot, (side==isRight ? 'R' : 'L'));
	if(side == isRight)
	{
		turnRight(90 - turnDeg, 15, tDia, GR, WB); //hardcoded the wheel base
		while(!isBlackLine_OA(lightArray, nBlack, right1))
		{
			motor[motorLeft] = 50 * ratMot;
			motor[motorRight] = 50;
		}
		/*
		turnRight(15, 15, tDia, GR, WB);
		*/
	}
	else if(side == isLeft)
	{
		turnLeft(90 - turnDeg, 15, tDia, GR, WB); //hardcoded the wheel base
		while(!isBlackLine_OA(lightArray, nBlack, left1))
		{
			motor[motorLeft] = 50;
			motor[motorRight] = 50 * ratMot;
		}
		/*
		turnLeft(15, 15, tDia, GR, WB);
		*/
	}

	nSyncedMotors = synchBC;
	pivotTurn(60, 20, tDia, GR, WB, side == isRight ? motorLeft : motorRight);
	{
		Sleep(50);
		motor[motorB] = -10;
		Update_MSLSA(lightArray);
		while (!(lightArray.percent[3] < lineThreshold && lightArray.percent[4] < lineThreshold))
		{
			Update_MSLSA(lightArray);
		}
			motor[motorB] = 0;
	}
	nSyncedMotors = synchNone;

	return;
}

void obstacleAvoidance()
{
	motorStop();
	int reverse = arduinoPing_readInt(Arduino, SensorFront) - (ObsDist);
	goStraight(reverse, 5, tDia, GR, WB);
	int deg=0;
	arduinoPing_setMode(Arduino, ARDUINO_PING_MODE_MEDIAN);
	enSide side = checkSide(Arduino);
	clearEncoders();
	float obsRadius = calObstDiam(side, &deg);
	nxtDisplayCenteredTextLine(3, "%f", obsRadius);
	goArd(obsRadius, side, deg);
 	//goArd(5.3, side);
	motorStop();
}
