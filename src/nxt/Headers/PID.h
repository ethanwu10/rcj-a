#ifndef PID_H
#define PID_H

void PIDCorrect()
{
	static float lastError = 0;
	static float cumError = 0;
	lastError = cumError = 0;
}

float PIDCorrect(float error, float PGain, float IGain, float DGain)
{
	static float lastError = 0;
	static float cumError = 0;
	float correction = 0;
	cumError += error;
	correction += error * PGain;
	correction += cumError * IGain;
	correction += (error - lastError) * DGain;
	lastError = error;
	return correction;
}

#endif //PID_H
