#pragma once

#include "vehicle.h"
#include "PID.h"
#include "arduinoPing.h"
#include "bitmap.h"
#include "mathFunc.h"



//const int tDist = 20;



const float hallWidth = 21.5;


task rampWFdown()
{
	SensorType[Arduino] = sensorI2CCustom;
	{
		ubyte ping_pwr_conf = 0;
		setBit(ping_pwr_conf, SensorLeft);
		setBit(ping_pwr_conf, SensorRight);
		arduinoPing_setConf(Arduino, ping_pwr_conf);
		arduinoPing_setMode(Arduino, ARDUINO_PING_MODE_FAST);
	}
	float error = 0;
	int correction = 0;
	int rampWFBasePower = -25;
	float PGain = 1;
	float IGain = 0;
	float DGain = 2;
	motor[motorLeft]  = 20;
	motor[motorRight] = 20;

	while(true)
	{
		float left, right;
		left  = arduinoPing_readFloat(Arduino, SensorLeft);
		right = arduinoPing_readFloat(Arduino, SensorRight);
		error = (left - right) * abs(left + right - hallWidth + 5.0);
		correction = PIDCorrect(error, PGain, IGain, DGain);
		if (abs(correction) < 150)
			break;
	}
	PlaySound(soundBeepBeep);
	while(true)
	{
		moveStraight(-20, 20, tDia, GR, WB);
		turn(25,20,tDia,GR,WB);
	}
	motor[motorLeft]  = 20;
	motor[motorRight] = 20;
}
