#ifndef MOVE_H
#define MOVE_H

#pragma systemFile

#include "sync.h"
#include "bitmap.h"

#define GOODSTATE 0

/******************************************************************************************************************************************************************************************************/

void clearEncoders(ubyte motors = 0b111);
void clearEncoders(bool MA, bool MB, bool MC);
void motorStop(ubyte motors = 0b111);
void motorStop(bool MA, bool MB, bool MC);
int moveStraight(float distance, int power, float tDia, float GR, float WB, TSynchedMotors synchMode = synchBC, tMotor motorMaster = motorB, bool desynchAfterComplete = true);
int turn(int deg, int power, float tDia, float GR, float WB, TSynchedMotors synchMode = synchBC, tMotor motorMaster = motorB, bool desynchAfterComplete = true);
int pivotTurn(int deg, int power, float tDia, float GR, float WB, tMotor targetMotor, bool resynchAfterComplete = true);
void turnMotor(tMotor targetMotor, int deg, int power = 50);


void clearEncoders(ubyte motors)
{
	TSynchedMotors oldSynchState = nSyncedMotors;
	nSyncedMotors = synchNone;

	if (readBit(motors, 0))
	{
		nMotorEncoder[motorA] = 0;
	}
	if (readBit(motors, 1))
	{
		nMotorEncoder[motorB] = 0;
	}
	if (readBit(motors, 2))
	{
		nMotorEncoder[motorC] = 0;
	}

	nSyncedMotors = oldSynchState;
}

void clearEncoders(bool MA, bool MB, bool MC)
{
	ubyte motors = 0;
	writeBit(motors, 0, MA);
	writeBit(motors, 1, MB);
	writeBit(motors, 2, MC);
	clearEncoders(motors);
}

void motorStop(ubyte motors)
{
	if (readBit(motors, 0))
	{
		motor[motorA] = 0;
	}
	if (readBit(motors, 1))
	{
		motor[motorB] = 0;
	}
	if (readBit(motors, 2))
	{
		motor[motorC] = 0;
	}
}

void motorStop(bool MA, bool MB, bool MC)
{
	ubyte motors = 0;
	writeBit(motors, 0, MA);
	writeBit(motors, 1, MB);
	writeBit(motors, 2, MC);
	motorStop(motors);
}

int getEnc(float distance, float tDia, float GR)
{
	float enc = distance * GR * (360/(tDia * PI));
	return round(enc);
}

float getDist(int deg, float tDia, float GR)
{
	return (deg * tDia * PI)/(GR * 360);
}

int getEnc_turn(int deg, float tDia, float GR, float WB)
{
	float enc = (WB / tDia) * GR * deg;
	return round(enc);
}

int getTurnDeg(int enc, float tDia, float GR, float WB)
{
	float deg = (enc * tDia) / (WB * GR);
	return round(deg);
}

int getEnc_pivotTurn(int deg, float tDia, float GR, float WB)
{
	float enc = (WB / (tDia / 2)) * deg * GR;
	return round(enc);
}

int getPivotTurnDeg(int enc, float tDia, float GR, float WB)
{
	float deg = (enc * (tDia / 2)) / (WB * GR);
	return round(deg);
}

int moveStraight(float distance, int power, float tDia, float GR, float WB, TSynchedMotors synchMode, tMotor motorMaster, bool desynchAfterComplete)
{
	TSynchedMotors oldSynchState = nSyncedMotors;
	int _power;
	float target;
	if (distance != 0)
	{
		nSyncedMotors = synchNone;
		{
			target = getEnc(abs(distance), tDia, GR);
			if (distance < 0)
			{
				_power = (power * -1);
			}
			else
			{
				_power = power;
			}
		}
		{
			if (synchMode == synchNone)
			{
				return -1;
			}
			else
			{
				clearEncoders();
				nSyncedMotors = synchMode;
				nSyncedTurnRatio = 100;
				nMotorEncoderTarget[motorMaster] = target;
				motor[motorMaster] = _power;
				while (!(nMotorRunState[motorMaster] == runStateIdle))
				{
					continue;
				}
				motor[motorMaster] = 0;
			}
		}
		{
			if (desynchAfterComplete == true)
			{
				nSyncedMotors = oldSynchState;
			}
		}
	}
	return GOODSTATE;
}

int turn(int deg, int power, float tDia, float GR, float WB, TSynchedMotors synchMode, tMotor motorMaster, bool desynchAfterComplete)
{
	TSynchedMotors oldSynchState = nSyncedMotors;
	int _power;
	float target;
	if (deg != 0)
	{
		nSyncedMotors = synchNone;
		{
			target = getEnc_turn(abs(deg), tDia, GR, WB);
			if (deg < 0)
			{
				_power = (power * -1);
			}
			else
			{
				_power = power;
			}
		}
		{
			if (synchMode == synchNone)
			{
				return -1;
			}
			else
			{
				clearEncoders();
				nSyncedMotors = synchMode;
				nSyncedTurnRatio = -100;
				nMotorEncoderTarget[motorMaster] = target;
				motor[motorMaster] = _power;
				while (!(nMotorRunState[motorMaster] == runStateIdle))
				{
					continue;
				}
				motor[motorMaster] = 0;
			}
		}
		{
			if (desynchAfterComplete)
			{
				nSyncedMotors = oldSynchState;
			}
		}
	}
	return GOODSTATE;
}

int pivotTurn(int deg, int power, float tDia, float GR, float WB, tMotor targetMotor, bool resynchAfterComplete)
{
	TSynchedMotors oldSynchState = nSyncedMotors;
	if (deg != 0)
	{
		nSyncedMotors = synchNone;
		{
			float target;
			target = getEnc_pivotTurn(deg, tDia, GR, WB);
			turnMotor(targetMotor, target, power);
		}
		{
			if (resynchAfterComplete)
			{
				nSyncedMotors = oldSynchState;
			}
		}
	}
	return GOODSTATE;
}

void turnMotor(tMotor targetMotor, int deg, int power)
{
	power = abs(power);
	if (deg < 0)
	{
		power *= -1;
	}
	deg = abs(deg);
	nMotorEncoder[targetMotor] = 0;
	nMotorEncoderTarget[targetMotor] = deg;
	motor[targetMotor] = power;
	while(nMotorRunState[targetMotor] != runStateIdle)
	{
	}
	motor[targetMotor] = 0;
	return;
}


#endif //MOVE_H
