#include "arduino.h"
#include "bitmap.h"

#define ARDUINO_REG_ALL_READINGS 0xff

void arduinoSensors_readAll(tSensors port, float* US_vals, bool* IR_val)
{
	memset(msg, 0, sizeof(msg));
	msg[0] = 2;
	msg[1] = ARDUINO_NXT_ADDR;
	msg[2] = ARDUINO_REG_ALL_READINGS;
	writeI2C(port, msg, reply, 7);

	for (int i = 0; i < 3; ++i)
	{
		US_vals[i] = reply[i] + (reply[(i + 3)] / 100.0);
	}
	*IR_val = readBit(reply[6], 0);
}

void arduinoSensors_readAll(tSensors port, float* S0_val, float* S1_val, float* S2_val, bool* IR_val)
{
	float _US_vals[3];
	arduinoSensors_readAll(port, _US_vals, IR_val);
	*S0_val = _US_vals[0];
	*S1_val = _US_vals[1];
	*S2_val = _US_vals[2];
}
