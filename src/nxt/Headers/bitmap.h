#pragma systemFile

void setBit(int& num, ubyte bit)
{
	num |= (1 << bit);
}

void unsetBit(int& num, ubyte bit)
{
	num &= (~(1 << bit));
}

void writeBit(int& num, ubyte bit, bool val)
{
	if (val == true)
	{
		setBit(num, bit);
	}
	else
	{
		unsetBit(num, bit);
	}
}

bool readBit(int num, ubyte bit)
{
	return (num & (1 << bit) >> bit);
}
