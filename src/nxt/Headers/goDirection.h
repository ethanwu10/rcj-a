#ifndef GODIRECTION_H
#define GODIRECTION_H

#include "move.h"

void goStraight(float dist, int power = 75, const float tDia, const float gr = 1, const float wb) //go forward or backwards for a specified distance in cm
{
	moveStraight(dist, power, tDia, gr, wb);
}

void turnLeft(int deg, int power = 65, const float tDia, const float gr = 1, const float wb) //point turn left for a specified degrees
{
	turn(deg, power, tDia, gr, wb, synchBC, motorB); //Hard-coding for now
	return;
}

void turnRight(int deg, int power = 65, const float tDia, const float gr = 1, const float wb) //point turn right for a specified degrees
{
	turn(deg, power, tDia, gr, wb, synchCB, motorC);
	return;
}

#endif
