#ifndef OBSTACLEAVOIDANCE_H
#define OBSTACLEAVOIDANCE_H

#include "PID.h"
#include "move.h"
#include "blackDetect.h"

#define OBSAVOID_DEBUG

const float tDia = 6.24;
const float gr = 1;
const float wb = 8.4;

const tSensors US = S4;
const tSensors MSLSA_port = S1;
const tMotor LM = motorB;
const tMotor RM = motorC;
const tMotor MH = motorA;

const int wallThresh = 20;
const int blackThresh = 20;

const int bPower = 30;
const float pGain = 0.9;
const float iGain = 0.03;
const float dGain = 0.09;
const int tDist = 12;

void PIDControl(int bPower, float tDist, float pGain, float dGain, float iGain, int side, bool debug = false)
{
	float correction;
	int dist;
	int error;
	dist = SensorValue[US];
	error = dist - tDist;
	correction = PIDCorrect(error, pGain, iGain, dGain);
	motor[LM] = bPower - (correction * side);
	motor[RM] = bPower + (correction * side);
	if (debug)
	{
		nxtDisplayCenteredTextLine(1, "%d - %d = %d", dist, tDist, error);
		nxtDisplayCenteredTextLine(3, "Left Motor- %d", bPower - correction);
		nxtDisplayCenteredTextLine(4, "Right Motor- %d", bPower + correction);
		nxtDisplayCenteredTextLine(6, "Correction - %d", correction);
		writeDebugStreamLine("%d - %d = %d", dist, tDist, error);
		writeDebugStreamLine("Left Motor- %d", bPower - correction);
		writeDebugStreamLine("Right Motor- %d", bPower + correction);
		writeDebugStreamLine("Correction - %d", correction);
	}
	wait10Msec(5);
	return;
}

void turnMH(int deg, int power = 50)
{
	power = abs(power);
	if (deg < 0)
	{
		power *= -1;
	}
	deg = abs(deg);
	nMotorEncoder[MH] = 0;
	nMotorEncoderTarget[MH] = deg;
	motor[MH] = power;
	while(nMotorRunState[MH] != runStateIdle)
	{
	}
	motor[MH] = 0;
	return;
}

bool obsAvoid_done;

task obstacleAvoidance()
{
	obsAvoid_done = false;
	turnMH(90);
	int side = SensorValue[US] < wallThresh ? 1 : -1;
	if (side == -1)
	{
		turnMH(-180);
	}
	turn(90 * side, 40, tDia, gr, wb);
	while(!isBlackLine(MSLSA_port, blackThresh))
	{
		PIDControl(bPower, tDist, pGain, dGain, iGain, side);
	}
	turnMH(-90 * side);
	obsAvoid_done = true;
}

#endif //OBSTACLEAVOIDANCE_H