#include "arduino.h"

#define ARDUINO_REG_IR_TIMEOUT 0x50
#define ARDUINO_REG_IR_VALUE   0x5f

void arduinoIR_setTimeout(tSensors port, ubyte timeout)
{
	writeArduinoReg_byte(port, ARDUINO_REG_IR_TIMEOUT, timeout);
}

ubyte arduinoIR_readTimeout(tSensors port)
{
	return readArduinoReg_byte(port, ARDUINO_REG_IR_TIMEOUT);
}

bool arduinoIR_readValue(tSensors port)
{
	return (readArduinoReg_byte(port, ARDUINO_REG_IR_VALUE) == 0xff);
}