#ifndef RAMPLF
#define RAMPLF

#include "vehicle.h"
#include "MSLSA.h"
#include "PID.h"
#include "mathFunc.h"


//#define MSLSALINEFOLLOW_SENSORFLIPPED

const float RLF_PGain = 4;
const float RLF_IGain = 0;
const float RLF_DGain = 5;

const int power = 30;

const float RLF_errorScale = 250;


task rampLF()
{
	Init_MSLSA(lightArray);
	WakeUp_MSLSA(lightArray);

	float error;
	float correction;
	float _error;

	while (true)
	{
		Update_MSLSA(lightArray);

#ifdef MSLSALINEFOLLOW_SENSORFLIPPED
		{
			byte cpyTmp_byte;
			int cpyTmp_int;
			for (int i = 0; i < 4; ++i)
			{
				int swapLoc = 7 - i;
				cpyTmp_byte = lightArray.percent[i];
				cpyTmp_int = lightArray.raw[i];
				lightArray.percent[i] = lightArray.percent[swapLoc];
				lightArray.raw[i] = lightArray.raw[swapLoc];
				lightArray.percent[swapLoc] = cpyTmp_byte;
				lightArray.raw[swapLoc] = cpyTmp_int;
			}
		}
#endif //MSLSALINEFOLLOW_SENSORFLIPPED

		_error = 0;
		for (int i = 0; i < 8; ++i)
		{
			lightArray.percent[i] = 100 - lightArray.percent[i];
		}
		for (int i = 1; i <= 4; ++i)
		{
			_error += ((i * 2) - 1) * ((lightArray.percent[4 - i]) - (lightArray.percent[3 + i]));
		}
		error = _error / RLF_errorScale;
		correction = PIDCorrect(error, RLF_PGain, RLF_IGain, RLF_DGain);
		motor[motorRight] = max(power - correction, 0);
		motor[motorLeft]  = max(power + correction, 0);
	}
}

#endif //RAMPLF
