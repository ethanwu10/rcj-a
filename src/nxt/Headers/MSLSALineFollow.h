#ifndef MSLSALINEFOLLOW_H
#define MSLSALINEFOLLOW_H

//Select bias mode
#define MSLSALINEFOLLOW_RIGHTBIAS
//#define MSLSALINEFOLLOW_LEFTBIAS

//Define to set sensor as flipped
//#define MSLSALINEFOLLOW_SENSORFLIPPED

//Define for human-readable debug
#define HUMANREADABLEDEBUG

//Define for CSV format debug
//#define CSVDEBUG

#include "MSLSA.h"
#include "PID.h"
#include "move.h"
#include "blackDetect.h"

const TSynchedMotors synchMode = synchBC;
const tMotor motorRight = motorB;
const tMotor motorLeft = motorC;
MSLSA lightArray = {S1};

const float leftWeight = 1;
const float rightWeight = 1;
const int basePower = 25;
const int slowPower = 20;
const int slowThreshold = 20;

const float PGain = 0.4;
const float IGain = 0;
const float DGain = 0.6;

#if defined(MSLSALINEFOLLOW_RIGHTBIAS) && defined(MSLSALINEFOLLOW_LEFTBIAS)
	#error "Too many bias modes selected"
#elif !defined(MSLSALINEFOLLOW_RIGHTBIAS) && !defined(MSLSALINEFOLLOW_LEFTBIAS)
	#error "No bias mode selected"
#else
	#ifdef MSLSALINEFOLLOW_RIGHTBIAS
		const float leftBiasedWeight = 3;
		const float rightBiasedWeight = 4.5;
	#endif //MSLSALINEFOLLOW_RIGHTBIAS

	#ifdef MSLSALINEFOLLOW_LEFTBIAS
		const float leftBiasedWeight = 5;
		const float rightBiasedWeight = 3;
	#endif //MSLSALINEFOLLOW_LEFTBIAS
#endif

const float errorScale = 25;

const short fastActionThreshold = 5;

const int lineThreshold = 30;
const short lineCntThreshold = 3;
const int whiteThreshold = 80;
const short whiteActionThreshold = 1;

const float tDia = 6.24;
const float GR = 1;
const float WB = 8.35;

const short turnDeg = 80;

void SearchForLine()
{
	TSynchedMotors oldSynchState = nSyncedMotors;
	nSyncedMotors = synchMode;
	nSyncedTurnRatio = -100;
	short searchPower = -15;
	int turnDist = getEnc_turn(turnDeg, tDia, GR, WB);
	nMotorEncoder[motorRight] = 0;
	nMotorEncoderTarget[motorRight] = turnDist;
	motor[motorRight] = searchPower;
	while (!isBlackLine(lightArray, lineThreshold))
	{
		if (nMotorRunState[motorRight] == runStateIdle)
		{
			motor[motorRight] = 0;
			searchPower *= -1;
			nMotorEncoder[motorRight] = 0;
			nMotorEncoderTarget[motorRight] = (turnDist * 2);
			motor[motorRight] = searchPower;
		}
	}
	nSyncedMotors = oldSynchState;
}

task lineFollow()
{
	clearDebugStream();
	Init_MSLSA(lightArray);
	MSLSAwakeUp(lightArray.port);
	if (Update_MSLSA(lightArray))
	{
		PlaySound(soundException);
		nxtDisplayCenteredTextLine(3, "MSLSA Read Error");
		Sleep(1000);
		StopAllTasks();
	}
	float error;
	float correction;
	float _error;
	float _leftWeight;
	float _rightWeight;
	bool line;
	short lastWhiteAmt = 0;
	short lastFastCnt = 0;
	int power = basePower;
	nNxtExitClicks = 2;
	while (nNxtButtonPressed != kExitButton)
	{
		//clearDebugStream();
		Update_MSLSA(lightArray);

#ifdef MSLSALINEFOLLOW_SENSORFLIPPED
	{
		byte cpyTmp_byte;
		int cpyTmp_int;
		for (int i = 0; i < 4; ++i)
		{
			int swapLoc = 7 - i;
			cpyTmp_byte = lightArray.percent[i];
			cpyTmp_int = lightArray.raw[i];
			lightArray.percent[i] = lightArray.percent[swapLoc];
			lightArray.raw[i] = lightArray.raw[swapLoc];
			lightArray.percent[swapLoc] = cpyTmp_byte;
			lightArray.raw[swapLoc] = cpyTmp_int;
		}
	}
#endif //MSLSALINEFOLLOW_SENSORFLIPPED

#ifdef CSVDEBUG
	{
		for(int i = 0; i < 8; ++i)
		{
			writeDebugStream("%d", lightArray.percent[i]);
			if (i < 7)
			{
				writeDebugStream(",");
			}
			else
			{
				writeDebugStream("\n");
			}
		}
//		writeDebugStreamLine("%d,%d,%d,%d,%d,%d,%d,%d", lightArray.percent[0], lightArray.percent[1], lightArray.percent[2], lightArray.percent[3], lightArray.percent[4], lightArray.percent[5], lightArray.percent[6], lightArray.percent[7]);
	}
#endif //CSVDEBUG

		_error = 0;
		{
			int lineCnt = 0;
			int whiteCnt = 0;
			for (int i = 0; i < 8; ++i)
			{
				if (lightArray.percent[i] < lineThreshold)
				{
					++lineCnt;
				}
				if (lightArray.percent[i] > whiteThreshold)
				{
					++whiteCnt;
				}
			}
			if (lineCnt > lineCntThreshold)
			{
				PlaySound(soundBlip);
				_rightWeight = rightBiasedWeight;
				_leftWeight = leftBiasedWeight;
			}
			else
			{
				_rightWeight = rightWeight;
				_leftWeight = leftWeight;
			}

			if (whiteCnt == 8)
			{
				++lastWhiteAmt;
			}
			else
			{
				lastWhiteAmt = 0;
			}
			if (lastWhiteAmt == 1)
			{
				clearEncoders();
			}
			if (lastWhiteAmt >= whiteActionThreshold)
			{
				line = false;
				if (nMotorEncoder[motorRight] > getEnc(20 + 1, tDia, GR))
				{
					SearchForLine();
				}
			}
			else
			{
				line = true;
			}
		}
		for (int i = 0; i < 8; ++i)
		{
			lightArray.percent[i] = 100 - lightArray.percent[i];
		}
		for (int i = 1; i <= 4; ++i)
		{
			_error += ((i * 2) - 1) * ((lightArray.percent[4 - i] * _rightWeight) - (lightArray.percent[3 + i] * _leftWeight));
		}
		error = _error / errorScale;
		{
			if (abs(error) > slowThreshold)
			{
				lastFastCnt = 0;
			}
			else
			{
				++lastFastCnt;
			}
			if (lastFastCnt > fastActionThreshold)
			{
				power = basePower;
				nxtDisplayCenteredBigTextLine(3, "Fast");
				//writeDebugStreamLine("Fast");
			}
			else
			{
				power = slowPower;
				//PlaySound(soundBlip);
				nxtDisplayCenteredBigTextLine(3, "Slow");
				//writeDebugStreamLine("Slow");
			}
		}
		correction = PIDCorrect(error, PGain, IGain, DGain);
		correction *= line;
#ifdef HUMANREADABLEDEBUG
		clearDebugStream();
		writeDebugStreamLine("Error:\t%.1f", error);
		writeDebugStreamLine("Correction:\t%.1f", correction);
		writeDebugStreamLine("Right:\t%d", power - correction);
		writeDebugStreamLine("Left:\t%d", power + correction);
#endif //HUMANREADABLEDEBUG
		motor[motorRight] = power - correction;
		motor[motorLeft] = power + correction;
	}
	//SaveNxtDatalog();
	Sleep_MSLSA(lightArray);
}

#endif //MSLSALINEFOLLOW_H
