#ifndef RAMP_H
#define RAMP_H

#include "vehicle.h"
//#include "arduinoPing.h"
#include "MPU6050.h"
#include "sync.h"
#include "PID.h"
#include "move.h"
#include "mathFunc.h"

//#include "rampLF.h"
#include "rampWF.h"
#include "rampWFdown.h"

#ifdef ETHAN
int XOffs = -378;
int YOffs = -3264;
int ZOffs =  1711;
#endif
#ifdef JOSEPH
int XOffs = -2269;
int YOffs = -1354;
int ZOffs =  1117;
#endif



const float rampThresh = tan(degreesToRadians(15));
const float noRampThresh = tan(degreesToRadians(10));
//const float rampThresh = cos(degreesToRadians(15));
//const float noRampThresh = cos(degreesToRadians(10));

const unsigned short rampActThresh = 5;
const unsigned short noRampActThresh = 2;
bool isRamp()
{

	static unsigned short rampCnt = 0;

	SensorType[MPU] = sensorI2CCustom;
	MPU6050_wakeUp(MPU);
	if (!MPU6050_testConnection(MPU))
	{
		PlaySound(soundException);
		nxtDisplayCenteredTextLine(4, "Comm failed");
		Sleep(1000);
		StopAllTasks();
	}

	{
		MPU6050_I2CRequest[0] = 4;
		MPU6050_I2CRequest[1] = MPU6050_NXT_ADDRESS;
		MPU6050_I2CRequest[2] = MPU6050_RA_XA_OFFS_H;
		MPU6050_I2CRequest[3] = (XOffs >> 8);   //MSB
		MPU6050_I2CRequest[4] = (XOffs & 0xFF); //LSB
		writeI2C(MPU, MPU6050_I2CRequest);

		MPU6050_I2CRequest[0] = 4;
		MPU6050_I2CRequest[1] = MPU6050_NXT_ADDRESS;
		MPU6050_I2CRequest[2] = MPU6050_RA_YA_OFFS_H;
		MPU6050_I2CRequest[3] = (YOffs >> 8);   //MSB
		MPU6050_I2CRequest[4] = (YOffs & 0xFF); //LSB
		writeI2C(MPU, MPU6050_I2CRequest);

		MPU6050_I2CRequest[0] = 4;
		MPU6050_I2CRequest[1] = MPU6050_NXT_ADDRESS;
		MPU6050_I2CRequest[2] = MPU6050_RA_ZA_OFFS_H;
		MPU6050_I2CRequest[3] = (ZOffs >> 8);   //MSB
		MPU6050_I2CRequest[4] = (ZOffs & 0xFF); //LSB
		writeI2C(MPU, MPU6050_I2CRequest);
	}


	int accelX, accelY, accelZ;
	MPU6050_readAccelX(MPU, accelX);
	MPU6050_readAccelY(MPU, accelY);
	MPU6050_readAccelZ(MPU, accelZ);
	float gX, gY, gZ;
	gX = accelX / 16384.0;
	gY = accelY / 16384.0;
	gZ = accelZ / 16384.0;
	nxtDisplayTextLine(2, "%.2f",gY);

	if(gY>0.1)
	{
		nxtDisplayTextLine(1, "down");
		moveStraight(-20,50,tDia,GR,WB);
		turn(180,50,tDia,GR,WB);
		moveStraight(-40,20,tDia,GR,WB);
	}

	/*{
	nxtDisplayTextLine(0, "Accel X:  % 6d", accelX);
	nxtDisplayTextLine(1, "Accel Y:  % 6d", accelY);
	nxtDisplayTextLine(2, "Accel Z:  % 6d", accelZ);

	writeDebugStreamLine("Accel X:  % 6d", accelX);
	writeDebugStreamLine("Accel Y:  % 6d", accelY);
	writeDebugStreamLine("Accel Z:  % 6d", accelZ);
	writeDebugStream("\n");
	}*/
	/*
	if (gY > 0)
	{
	rampCnt = 0;
	}
	else
	*/
	{
		//if (gZ < rampThresh)
		if ((sqrt(sqr(gX) + sqr(gY)) / gZ) > rampThresh)
		{
			rampCnt++;
		}
		else
		{
			rampCnt = 0;
		}
	}
	return (rampCnt > rampActThresh);
}
void rampback()
{
	SensorType[MPU] = sensorI2CCustom;
	nSyncedMotors = synchNone;
	MPU6050_wakeUp(MPU);
	if (!MPU6050_testConnection(MPU))
	{
		PlaySound(soundException);
		nxtDisplayCenteredTextLine(4, "Comm failed");
		Sleep(1000);
		StopAllTasks();
	}

	{
		MPU6050_I2CRequest[0] = 4;
		MPU6050_I2CRequest[1] = MPU6050_NXT_ADDRESS;
		MPU6050_I2CRequest[2] = MPU6050_RA_XA_OFFS_H;
		MPU6050_I2CRequest[3] = (XOffs >> 8);   //MSB
		MPU6050_I2CRequest[4] = (XOffs & 0xFF); //LSB
		writeI2C(MPU, MPU6050_I2CRequest);

		MPU6050_I2CRequest[0] = 4;
		MPU6050_I2CRequest[1] = MPU6050_NXT_ADDRESS;
		MPU6050_I2CRequest[2] = MPU6050_RA_YA_OFFS_H;
		MPU6050_I2CRequest[3] = (YOffs >> 8);   //MSB
		MPU6050_I2CRequest[4] = (YOffs & 0xFF); //LSB
		writeI2C(MPU, MPU6050_I2CRequest);

		MPU6050_I2CRequest[0] = 4;
		MPU6050_I2CRequest[1] = MPU6050_NXT_ADDRESS;
		MPU6050_I2CRequest[2] = MPU6050_RA_ZA_OFFS_H;
		MPU6050_I2CRequest[3] = (ZOffs >> 8);   //MSB
		MPU6050_I2CRequest[4] = (ZOffs & 0xFF); //LSB
		writeI2C(MPU, MPU6050_I2CRequest);
	}
	unsigned short noRampCnt = 0;



	int accelX, accelY, accelZ;
	MPU6050_readAccelX(MPU, accelX);
	MPU6050_readAccelY(MPU, accelY);
	MPU6050_readAccelZ(MPU, accelZ);
	float gX, gY, gZ;
	gX = accelX / 16384.0;
	gY = accelY / 16384.0;
	gZ = accelZ / 16384.0;


	int t=0;
	if(gY>0.2)
	{
		nxtDisplayTextLine(1, "down");
		t=1;
	}


	if(t==1)
		StartTask(rampWFdown);
	if(t==0)
		StartTask(rampWF);
	while (true)
	{
		int accelX, accelY, accelZ;
		MPU6050_readAccelX(MPU, accelX);
		MPU6050_readAccelY(MPU, accelY);
		MPU6050_readAccelZ(MPU, accelZ);
		float gX, gY, gZ;
		gX = accelX / 16384.0;
		gY = accelY / 16384.0;
		gZ = accelZ / 16384.0;
		{
			/*{
			nxtDisplayTextLine(0, "Accel X:  % 6d", accelX);
			nxtDisplayTextLine(1, "Accel Y:  % 6d", accelY);
			nxtDisplayTextLine(2, "Accel Z:  % 6d", accelZ);

			writeDebugStreamLine("Accel X:  % 6d", accelX);
			writeDebugStreamLine("Accel Y:  % 6d", accelY);
			writeDebugStreamLine("Accel Z:  % 6d", accelZ);
			writeDebugStream("\n");
			}*/
			//if (gZ > noRampThresh)
			if ((sqrt(sqr(gX) + sqr(gY)) / gZ) < noRampThresh)
			{
				noRampCnt++;
			}
			else
			{
				noRampCnt = 0;
			}
			if (noRampCnt > noRampActThresh)
				break;
		}
	}
	if(t==0)
		StopTask(rampWF);
	if(t==1)
	{
		StopTask(rampWFdown);
		moveStraight(-10,20,tDia,GR,WB);
		moveStraight(5,20,tDia,GR,WB);
		turn(-90,50,tDia,GR,WB);
		moveStraight(13,20,tDia,GR,WB);
	}
	Sleep(250);
	moveStraight(-13, 20, tDia, GR, WB);
}

#endif //RAMP_H
