#ifndef MSLSALINEFOLLOW_H
#define MSLSALINEFOLLOW_H

//Select bias mode
#define MSLSALINEFOLLOW_RIGHTBIAS
//#define MSLSALINEFOLLOW_LEFTBIAS

//Define to set sensor as flipped
//#define MSLSALINEFOLLOW_SENSORFLIPPED

//Define for human-readable debug
//#define HUMANREADABLEDEBUG

//Define for CSV format debug
//#define CSVDEBUG

#include "vehicle.h"
#include "MSLSA.h"
#include "PID.h"
#include "move.h"
#include "blackDetect.h"
#include "arduinoPing.h"


const TSynchedMotors synchMode = synchBC;

const int basePower = 20;
const int slowPower = 20;
const int slowThreshold = 20;

const float LF_PGain = 4;
const float LF_IGain = 0;
const float LF_DGain = 4;

#if defined(MSLSALINEFOLLOW_RIGHTBIAS) && defined(MSLSALINEFOLLOW_LEFTBIAS)
	#error "Too many bias modes selected"
#elif !defined(MSLSALINEFOLLOW_RIGHTBIAS) && !defined(MSLSALINEFOLLOW_LEFTBIAS)
	#error "No bias mode selected"
#else
	#ifdef MSLSALINEFOLLOW_RIGHTBIAS
		const ubyte pfdSensor = 0;  //Array index for preferred sensor; turns will prefer this side if line is present on both sensors
		const ubyte npfdSensor = 7; //Array index for non-preferred sensor; robot will only turn to this side if no line is present on preferred sensor and no line continuing straight is present
		const tMotor pfdTurnMotor = motorLeft;
		const tMotor npfdTurnMotor = motorRight;
	#endif //MSLSALINEFOLLOW_RIGHTBIAS

	#ifdef MSLSALINEFOLLOW_LEFTBIAS
		const ubyte pfdSensor = 7;  //Array index for preferred sensor; turns will prefer this side if line is present on both sensors
		const ubyte npfdSensor = 0; //Array index for non-preferred sensor; robot will only turn to this side if no line is present on preferred sensor and no line continuing straight is present
		const tMotor pfdTurnMotor = motorRight;
		const tMotor npfdTurnMotor = motorLeft;
	#endif //MSLSALINEFOLLOW_LEFTBIAS
#endif

const float LF_errorScale = 250;

const short fastActionThreshold = 5;

const int lineThreshold = 10;
const short lineCntThreshold = 4;
const short lineActionThreshold = 2;
const int whiteThreshold = 80;
const short whiteActionThreshold = 1;

const short turnDeg = 45;
const unsigned short searchPower = 15;
const unsigned short searchForwardDist = 2;
const unsigned short gapDist_noWall = 20/*cm*/;
const unsigned short gapDist_Wall = 30/*cm*/;

const unsigned short alongWallThresh = 20/*cm*/;

const float backupDist = -3/*cm*/;
const float WheeltoSensorDist = 5/*cm*/;


typedef enum {LineFollowState_Normal, LineFollowState_Intersection, LineFollowState_Gap, LineFollowState_Idle} TLineFollowState;

TLineFollowState LineFollowState = LineFollowState_Idle;


signed short side = 1;
task __searchForLine()
{
	while (true)
	{
		signed short direction = 1;
		side = 1;
		for (int i = 0; i < 2; i++)
		{
			tMotor turningMotor = side == 1 ? motorB : motorC;
			{
				turn(turnDeg * side, 20, tDia, GR, WB);
				moveStraight(7.5, searchPower, tDia, GR, WB);
				moveStraight(-7.5, searchPower, tDia, GR, WB);
				turn(turnDeg * side * -1, 20, tDia, GR, WB);
			}
			side *= -1;
		}
		moveStraight(searchForwardDist, searchPower, tDia, GR, WB);
	}
}

void SearchForLine()
{
	{
		motor[motorB] = motor[motorC] = 0; //Stop motors
		StartTask(__searchForLine);
		while (!isBlackLine(lightArray, lineThreshold))
		{
		}
		StopTask(__searchForLine);
		nSyncedMotors = synchNone;
	}

	/*{
		TSynchedMotors oldSynchState = nSyncedMotors;
		nSyncedMotors = synchMode;
		nSyncedTurnRatio = 100;
		motor[motorRight] = 10;
		while (!(lightArray.percent[3] < lineThreshold && lightArray.percent[4] < lineThreshold))
		{
			Update_MSLSA(lightArray);
		}
		motor[motorRight] = 0;
		nSyncedMotors = oldSynchState;
	}*/

	//pivotTurn(turnDeg, 20, tDia, GR, WB, side == 1 ? motorC : motorB);
}

void Intersection_backUp()
{
	moveStraight(backupDist, 20, tDia, GR, WB);
}

void Intersection()
{
	LineFollowState = LineFollowState_Intersection;
	if (lightArray.percent[pfdSensor] < lineThreshold)
	{
		moveStraight(WheeltoSensorDist, 10, tDia, GR, WB);
		//pivotTurn(90, 25, tDia, GR, WB, pfdTurnMotor);
		turn(90, 25, tDia, GR, WB, getSyncMode(pfdTurnMotor, npfdTurnMotor), pfdTurnMotor);
		Intersection_backUp();
	}
	else
	{
		bool nfpdFound = false;
		for(int i = 0; i < 3; i++)
		{
			moveStraight(0.3, 10, tDia, GR, WB);
			Update_MSLSA(lightArray);
			if (lightArray.percent[pfdSensor] < lineThreshold)
			{
				moveStraight((-0.3 * i) + WheeltoSensorDist, 10, tDia, GR, WB);
				//pivotTurn(90, 25, tDia, GR, WB, pfdTurnMotor);
				turn(90, 25, tDia, GR, WB, getSyncMode(pfdTurnMotor, npfdTurnMotor), pfdTurnMotor);
				Intersection_backUp();
				nfpdFound = false;
				break;
			}
			else if (lightArray.percent[npfdSensor] < lineThreshold)
			{
				nfpdFound = true;
			}
		}
		if (nfpdFound)
		{
			moveStraight(2.5, 10, tDia, GR, WB);
			if (isBlackLine(lightArray, lineThreshold))
			{
			}
			else
			{
				moveStraight(-4 + WheeltoSensorDist, 10, tDia, GR, WB);
				//pivotTurn(90, 25, tDia, GR, WB, npfdTurnMotor);
				turn(90, 25, tDia, GR, WB, getSyncMode(npfdTurnMotor, pfdTurnMotor), npfdTurnMotor);
				Intersection_backUp();
			}
		}
	}
	LineFollowState = LineFollowState_Normal;
}

void Gap()
{
	LineFollowState = LineFollowState_Gap;
	motor[motorRight] = motor[motorLeft] = 0;
	Sleep(100);
	unsigned short _gapDist = ((arduinoPing_readInt(Arduino, SensorRight) < alongWallThresh) || (arduinoPing_readInt(Arduino, SensorLeft) < alongWallThresh)) ? gapDist_Wall : gapDist_noWall;
	motor[motorRight] = motor[motorLeft] = basePower;
	while (!isBlackLine(lightArray, lineThreshold))
	{
		if (nMotorEncoder[motorRight] > getEnc(_gapDist + 2, tDia, GR))
		{
			SearchForLine();
			break;
		}
	}
	LineFollowState = LineFollowState_Normal;
}

task lineFollow()
{
	clearDebugStream();
	Init_MSLSA(lightArray);
	WakeUp_MSLSA(lightArray);
	SensorType[Arduino] = sensorI2CCustom;
	{
		ubyte ping_pwr_conf = 0;
		setBit(ping_pwr_conf, SensorLeft);
		setBit(ping_pwr_conf, SensorRight);
		arduinoPing_setConf(Arduino, ping_pwr_conf);
	}
	if (Update_MSLSA(lightArray))
	{
		PlaySound(soundException);
		nxtDisplayCenteredTextLine(3, "MSLSA Read Error");
		Sleep(1000);
		StopAllTasks();
	}
	float error;
	float correction;
	float _error;
	bool line;
	short lastWhiteAmt = 0;
	short lastLineCnt = 0;
	short lastFastCnt = 0;
	int power = basePower;
	//nNxtExitClicks = 2;
	while (nNxtButtonPressed != kExitButton)
	{
		//clearDebugStream();
		Update_MSLSA(lightArray);
		LineFollowState = LineFollowState_Normal;

#ifdef MSLSALINEFOLLOW_SENSORFLIPPED
	{
		byte cpyTmp_byte;
		int cpyTmp_int;
		for (int i = 0; i < 4; ++i)
		{
			int swapLoc = 7 - i;
			cpyTmp_byte = lightArray.percent[i];
			cpyTmp_int = lightArray.raw[i];
			lightArray.percent[i] = lightArray.percent[swapLoc];
			lightArray.raw[i] = lightArray.raw[swapLoc];
			lightArray.percent[swapLoc] = cpyTmp_byte;
			lightArray.raw[swapLoc] = cpyTmp_int;
		}
	}
#endif //MSLSALINEFOLLOW_SENSORFLIPPED

#ifdef CSVDEBUG
	{
		for(int i = 0; i < 8; ++i)
		{
			writeDebugStream("%d", lightArray.percent[i]);
			if (i < 7)
			{
				writeDebugStream(",");
			}
			else
			{
				writeDebugStream("\n");
			}
		}
	}
#endif //CSVDEBUG

		_error = 0;
		{
			int lineCnt = 0;
			int whiteCnt = 0;
			for (int i = 0; i < 8; ++i)
			{
				if (lightArray.percent[i] < lineThreshold)
				{
					++lineCnt;
				}
				if (lightArray.percent[i] > whiteThreshold)
				{
					++whiteCnt;
				}
			}


/*INTERSECTION******************************************/
			if (lineCnt > lineCntThreshold)
			{
				++lastLineCnt;
			}
			else
			{
				lastLineCnt = 0;
			}
			if (lastLineCnt > lineActionThreshold)
			{
				//PlaySound(soundBlip);

				Intersection();
			}
/*******************************************************/

/*GAP***************************************************/
			if (whiteCnt == 8)
			{
				++lastWhiteAmt;
			}
			else
			{
				lastWhiteAmt = 0;
			}
			if (lastWhiteAmt == 1)
			{
				clearEncoders();
			}

			if (lastWhiteAmt >= whiteActionThreshold)
			{
				line = false;
				Gap();
			}
			else
			{
				line = true;
			}
/*******************************************************/
		}
		for (int i = 0; i < 8; ++i)
		{
			lightArray.percent[i] = 100 - lightArray.percent[i];
		}
		for (int i = 1; i <= 4; ++i)
		{
			_error += ((i * 2) - 1) * ((lightArray.percent[4 - i]) - (lightArray.percent[3 + i]));
		}
		error = _error / LF_errorScale;
		{
			if (abs(error) > slowThreshold)
			{
				lastFastCnt = 0;
			}
			else
			{
				++lastFastCnt;
			}
			if (lastFastCnt > fastActionThreshold)
			{
				power = basePower;
				//nxtDisplayCenteredBigTextLine(3, "Fast");
				//writeDebugStreamLine("Fast");
			}
			else
			{
				power = slowPower;
				//PlaySound(soundBlip);
				//nxtDisplayCenteredBigTextLine(3, "Slow");
				//writeDebugStreamLine("Slow");
			}
		}
		correction = PIDCorrect(error, LF_PGain, LF_IGain, LF_DGain);
		correction *= line;
#ifdef HUMANREADABLEDEBUG
		clearDebugStream();
		writeDebugStreamLine("Error:\t%.1f", error);
		writeDebugStreamLine("Correction:\t%.1f", correction);
		writeDebugStreamLine("Right:\t%d", power - correction);
		writeDebugStreamLine("Left:\t%d", power + correction);
#endif //HUMANREADABLEDEBUG
		motor[motorRight] = power - correction;
		motor[motorLeft] = power + correction;
	}
	//SaveNxtDatalog();
	Sleep_MSLSA(lightArray);
	LineFollowState = LineFollowState_Idle;
}

#endif //MSLSALINEFOLLOW_H
