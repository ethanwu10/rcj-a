#ifndef EVACROOM_H
#define EVACROOM_H


#include "vehicle.h"
#include "move.h"
#include "arduinoPing.h"
#include "arduinoIR.h"
#include "MSLSA.h"
#include "blackDetect.h"
#include "bitmap.h"
#include "mathFunc.h"


const unsigned short moveDist = 10/*cm*/;

const unsigned short blackThresh = 10;


const TSynchedMotors synchMode = synchBC;


void straighten(unsigned short moveDist)
{
	ubyte oldConf = arduinoPing_getConf(Arduino);
	ubyte newConf;
	newConf = oldConf | 0b001; //Hard coded for now
	arduinoPing_setConf(Arduino, newConf);
	float distsLeft[2];
	float distsRight[2];
	Sleep(500);
	distsLeft[0] = arduinoPing_readFloat(Arduino, SensorLeft);
	distsRight[0] = arduinoPing_readFloat(Arduino, SensorRight);
	moveStraight(moveDist, 20, tDia, GR, WB);
	Sleep(500);
	distsLeft[1] = arduinoPing_readFloat(Arduino, SensorLeft);
	distsRight[1] = arduinoPing_readFloat(Arduino, SensorRight);
	float turnDegLeft = radiansToDegrees(atan((distsLeft[1] - distsLeft[0]) / (float)moveDist));
	float turnDegRight = radiansToDegrees(atan((distsRight[1] - distsRight[0]) / (float)moveDist));
	if (turnDegLeft > 180)
	{
		turnDegLeft -= 360;
	}
	if (turnDegRight > 180)
	{
		turnDegRight -= 360;
	}
	turnDegRight *= -1; //Flip sign so that the turn measures are equivalent
	writeDebugStreamLine("% 5.2f\t% 5.2f", turnDegLeft, turnDegLeft);
	float turnDeg = abs(turnDegRight) < abs(turnDegLeft) ? turnDegRight : turnDegLeft;
	turn(turnDeg, 10, tDia, GR, WB);
	arduinoPing_setConf(Arduino, oldConf);
}

void straightenLeft(unsigned short moveDist)
{
        ubyte oldConf = arduinoPing_getConf(Arduino);
        ubyte newConf;
        newConf = oldConf | 0b001; //Hard coded for now
        arduinoPing_setConf(Arduino, newConf);
        float dists[2];
        Sleep(500);
        dists[0] = arduinoPing_readFloat(Arduino, SensorLeft);
        moveStraight(moveDist, 20, tDia, GR, WB);
        Sleep(500);
        dists[1] = arduinoPing_readFloat(Arduino, SensorLeft);
        float turnDeg = radiansToDegrees(atan((dists[1] - dists[0]) / (float)moveDist));
        if (turnDeg > 180)
        {
                turnDeg -= 360;
        }
        writeDebugStreamLine("% 5.2f", turnDeg);
        turn(turnDeg, 10, tDia, GR, WB);
        arduinoPing_setConf(Arduino, oldConf);
}


void getVictim(ubyte threshold, unsigned short length, int victimLoc)
{
	writeDebugStreamLine("%d", threshold);
	//moveStraight(-1 * moveDist, 20, tDia, GR, WB);
	arduinoPing_setMode(Arduino, ARDUINO_PING_MODE_MEDIAN);
	{
		arduinoPing_setConf(Arduino, 0b111);
	}
	arduinoPing_setModeCfg(Arduino, 3);

	nSyncedMotors = synchMode;
	Sleep(500);
	short dist;
	bool isVictimInPath = false;
	if (!(arduinoPing_readInt(Arduino, SensorFront) < (victimLoc + 5)))
	{
		if (!(arduinoPing_readInt(Arduino, SensorRight) < threshold || arduinoPing_readInt(Arduino, SensorFront) < 20))
			straighten(moveDist);
		moveStraight(max(victimLoc - 5, 0), 20, tDia, GR, WB);
		if (arduinoPing_readInt(Arduino, SensorFront) < 5)
		{
			isVictimInPath = true;
		}
		if (!isVictimInPath)
		{
			Sleep(500);
			if (arduinoPing_readInt(Arduino, SensorRight) < 10)
			{
				pivotTurn(-90, 10, tDia, GR, WB, motorRight);
			}
			else
			{
				turn(-90, 15, tDia, GR, WB);
				straighten(10);
			}
		}
	}
	else
	{
		if (!(arduinoPing_readInt(Arduino, SensorRight) < threshold || arduinoPing_readInt(Arduino, SensorFront) < 20))
			straighten(moveDist);
		motor[motorB] = 10;
		while (!(arduinoPing_readInt(Arduino, SensorRight) < threshold))
		{
			if (arduinoPing_readInt(Arduino, SensorFront) < 5)
			{
				isVictimInPath = true;
				break;
			}
		}
		if (!isVictimInPath)
		{
			motor[motorB] = 0;
			clearEncoders();
			PlaySound(soundBeepBeep);
			motor[motorB] = 10;
			Sleep(2000);
			while (!(arduinoPing_readInt(Arduino, SensorRight) > threshold || arduinoPing_readInt(Arduino, SensorFront) < 5 || arduinoIR_readValue(Arduino)));
			dist = nMotorEncoder[motorB];
			motor[motorB] = 0;
			clearEncoders();
			arduinoPing_setConf(Arduino, 0b111);
			PlaySound(soundBeepBeep);
			nMotorEncoderTarget[motorB] = dist / 2;
			motor[motorB] = -20;
			while (!(nMotorRunState[motorB] == runStateIdle));
			motor[motorB] = 0;
			Sleep(500);
			if (arduinoPing_readInt(Arduino, SensorRight) < 5)
			{
				pivotTurn(-90, 10, tDia, GR, WB, motorRight);
			}
			else
			{
				turn(-90, 20, tDia, GR, WB);
			}
		}
	}
	bool victimOnLeft = false;
	bool victimOnRight = false;
	if (!isVictimInPath)
	{
		arduinoPing_setMode(Arduino, ARDUINO_PING_MODE_MEDIAN);
		motor[motorB] = 20;
		bool victimInPath = true;
		int lastDist = 255;
		int currDist;
		unsigned short lostVictCount = 0;
		int threshLeft = min((arduinoPing_readInt(Arduino, SensorLeft) - 5), 10);
		int threshRight = min((arduinoPing_readInt(Arduino, SensorRight) - 5), 10);
		while (!(arduinoPing_readInt(Arduino, SensorFront) < 5))
		{
			if (arduinoPing_readInt(Arduino, SensorLeft) < threshLeft)
			{
				victimOnLeft = true;
				victimInPath = false;
				break;
			}
			if ((currDist = arduinoPing_readInt(Arduino, SensorRight)) < threshRight)
			{
				victimOnRight = true;
				victimInPath = false;
				dist = lastDist - 25;
				break;
			}
			lastDist = currDist;
		}
		if (victimInPath)
		{
			Sleep(250);
			motor[motorB] = 0;
			dist = arduinoPing_readInt(Arduino, SensorRight) - 25;
		}
		else
		{
			moveStraight(3, 15, tDia, GR, WB);
			Sleep(250);
			if (arduinoPing_readInt(Arduino, victimOnRight ? SensorRight : SensorLeft) < 5)
			{
				pivotTurn(-90, 10, tDia, GR, WB, victimOnRight ? motorRight :  motorLeft);
			}
			else
			{
				turn(90 * (victimOnLeft ? 1 : -1), 20, tDia, GR, WB);
			}
			motor[motorB] = 10;
			while (!(arduinoPing_readInt(Arduino, SensorFront) < 5));
			Sleep(250);
			motor[motorB] = 0;
		}
	}
	else
	{
		Sleep(250);
		motor[motorB] = 0;
		pivotTurn(100, 20, tDia, GR, WB, motorLeft);
		pivotTurn(-10, 20, tDia, GR, WB, motorLeft);
		moveStraight(10, 20, tDia, GR, WB);
		Sleep(250);
		dist = arduinoPing_readInt(Arduino, SensorRight) - 25;
	}
	if ((dist < 0) && !victimOnLeft)
	{
		pivotTurn(100, 20, tDia, GR, WB, motorRight);
		pivotTurn(-10, 20, tDia, GR, WB, motorRight);
	}
	if ((dist < 0) || victimOnLeft)
	{
		Sleep(250);
		dist = arduinoPing_readInt(Arduino, SensorLeft) - 25;
		pivotTurn(100, 20, tDia, GR, WB, motorRight);
		pivotTurn(-10, 20, tDia, GR, WB, motorRight);
		if (dist > 10)
		{
			straighten(10);
			moveStraight(max(dist - 10, 0), 20, tDia, GR, WB);
		}
		else
		{
			moveStraight(max(dist, 0), 20, tDia, GR, WB);
		}
		Sleep(250);
		dist = arduinoPing_readInt(Arduino, SensorLeft) - 17;
		pivotTurn(90, 20, tDia, GR, WB, motorRight);
		moveStraight(dist, 20, tDia, GR, WB);
		pivotTurn(90, 20, tDia, GR, WB, motorLeft);
		moveStraight(-20, 20, tDia, GR, WB);
	}
	else
	{
		if (!victimOnRight)
		{
			pivotTurn(100, 20, tDia, GR, WB, motorLeft);
			pivotTurn(-10, 20, tDia, GR, WB, motorLeft);
		}
		if (dist > 20)
		{
			straighten(10);
			moveStraight(dist - 10, 20, tDia, GR, WB);
		}
		else
		{
			moveStraight(max(dist, 0), 20, tDia, GR, WB);
		}
		dist = arduinoPing_readInt(Arduino, SensorRight) - 17;
		pivotTurn(100, 20, tDia, GR, WB, motorLeft);
		pivotTurn(-10, 20, tDia, GR, WB, motorLeft);
		straighten(10);
		moveStraight(dist - 10, 20, tDia, GR, WB);
		pivotTurn(90, 20, tDia, GR, WB, motorRight);
		moveStraight(-20, 20, tDia, GR, WB);
	}
}

void pushVictimAway()
{
	pivotTurn(90, 20, tDia, GR, WB, motorLeft);
	moveStraight(20, 20, tDia, GR, WB);
	moveStraight(-20, 20, tDia, GR, WB);
	pivotTurn(-90, 20, tDia, GR, WB, motorLeft);
	moveStraight(-3, 10, tDia, GR, WB);
}

void evacRoom()
{
	moveStraight(10, 20, tDia, GR, WB);
	SensorType[Arduino] = sensorI2CCustom;
	arduinoPing_setMode(Arduino, ARDUINO_PING_MODE_MEDIAN);
	arduinoPing_setConf(Arduino, 0b111);
	unsigned short width = 0;
	unsigned short length = 0;
	unsigned short compensation = 0;
	bool locUnknown = false;
	Sleep(500);
	{
		length = arduinoPing_readInt(Arduino, SensorFront) > 100 ? 120 : 90;
		width = (arduinoPing_readInt(Arduino, SensorLeft) + arduinoPing_readInt(Arduino, SensorRight)) > 100 ? 120 : 90;
		if (length == width)
			locUnknown = true;
		if (arduinoPing_readInt(Arduino, SensorLeft) > 25)
		{
			{//swap lengths and widths
				unsigned short tmp = length;
				length = width;
				width = tmp;
			}
			compensation = 20 + (arduinoPing_readInt(Arduino, SensorRight) - 10);
			moveStraight(15, 20, tDia, GR, WB);
			turn(90, 20, tDia, GR, WB);
			moveStraight(20, 20, tDia, GR, WB);
		}
	}
	{
		ubyte conf = 0;
		setBit(conf, SensorRight);
		setBit(conf, SensorLeft);
		setBit(conf, SensorFront);
		arduinoPing_setConf(Arduino, conf);
		Sleep(500);
	}
	int victimLoc = 255;
	{
		{
			bool foundEvacPoint = false;
			MSLSA lightArray;
			lightArray.port = S1;
			Init_MSLSA(lightArray);
			bool isVictimInPath = false;
			unsigned short victimDist = 0;

			isVictimInPath = arduinoPing_readInt(Arduino, SensorFront) < (length - 35 - compensation);
			if (isVictimInPath)
				victimDist = arduinoPing_readInt(Arduino, SensorFront);
			else
			{
				if (arduinoPing_readInt(Arduino, SensorFront) == 255)
				{
					isVictimInPath = true;
					victimDist = 15;
				}
			}
			victimDist = max(victimDist, 15);
			while (!foundEvacPoint)
			{
				victimLoc = 255;
				straightenLeft(moveDist);
				clearEncoders();
				nSyncedMotors = synchMode;
				nSyncedTurnRatio = 100;
				motor[motorB] = 20;
				while (arduinoPing_readInt(Arduino, SensorFront) > 8)
				{
					int reading = arduinoPing_readInt(Arduino, SensorRight);
					if (reading < victimLoc)
					{
						victimLoc = reading;
					}
				}
				motor[motorB] = 0;
				if (locUnknown)
				{
					width = (arduinoPing_readInt(Arduino, SensorLeft) + arduinoPing_readInt(Arduino, SensorRight)) > 100 ? 120 : 90;
					if (width == 120)
					{
						isVictimInPath = false;
					}
					else
					{
						isVictimInPath = true;
					}
				}
				if (isVictimInPath && (nMotorEncoder[motorB] < getEnc(victimDist - 5, tDia, GR)))
				{
					motor[motorB] = 10;
					while (arduinoPing_readInt(Arduino, SensorFront) > 5); //get closer
					Sleep(250);
					motor[motorB] = 0;
					pushVictimAway();
					isVictimInPath = false;
					continue;
				}
				Sleep(500);
				if (isBlackLine(lightArray, blackThresh))
				{
					foundEvacPoint = true;
				}
				else
				{
					if (arduinoPing_readInt(Arduino, SensorLeft) > 18)
					{
						turn(-30, 20, tDia, GR, WB);
						moveStraight(-10, 20, tDia, GR, WB);
						turn(30, 20, tDia, GR, WB);
						moveStraight(10, 20, tDia, GR, WB);
						if (isBlackLine(lightArray, blackThresh))
						{
							foundEvacPoint = true;
						}
					}
				}
				unsigned short turnAwayDeg = arduinoPing_readInt(Arduino, SensorLeft) > 7 ? 0 : 15;
				turn(turnAwayDeg, 20, tDia, GR, WB);
				moveStraight(-12 + (foundEvacPoint ? -2 : 0), 20, tDia, GR, WB);
				pivotTurn(90 + turnAwayDeg, 20, tDia, GR, WB, motorLeft);
				{//swap lengths and widths
					unsigned short tmp = length;
					length = width;
					width = tmp;
				}
				clearEncoders();
				Sleep(500);

				isVictimInPath = arduinoPing_readInt(Arduino, SensorFront) < (length - 35);
				if (isVictimInPath)
					victimDist = arduinoPing_readInt(Arduino, SensorFront);
				else
				{
					if (arduinoPing_readInt(Arduino, SensorFront) == 255)
					{
						isVictimInPath = true;
						victimDist = 15;
					}
					else
					{
						victimDist = 0;
					}
				}
			victimDist = max(victimDist, 15);
			}
		}
		Sleep(500);
		getVictim(width - 5 - 9 - arduinoPing_readInt(Arduino, SensorLeft), length, victimLoc);
	}
}

#endif //EVACROOM_H
