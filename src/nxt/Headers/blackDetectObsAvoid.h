#ifndef BLACKDETECT_OBSAVOID_H
#define BLACKDETECT_OBSAVOID_H

#include "MSLSA.h"

typedef enumWord MSLSALoc
{
	left      = 0,
	left1     = 1,
	left2     = 2,
	midLeft   = 3,
	midRight  = 4,
	right2    = 5,
	right1    = 6,
	right     = 7,
	all       = 8
}MSLSALoc;

////////////////////////////////
//	 __ __ __ __ __ __ __ __	//
//	|0 |1 |2 |3 |4 |5 |6 |7 |	//
//	|__|__|__|__|__|__|__|__|	//
//														//
////////////////////////////////

bool isBlackLine_OA(MSLSA link, int blackThresh, MSLSALoc i)
{
	Init_MSLSA(link);
	MSLSAwakeUp(link.port);
	Update_MSLSA(link);
	bool isBlack = false;
	if(i == all)
	{
		for(int j = 0; j < 7; j++)
		{
			if(link.percent[i] < blackThresh)
			{
				isBlack = true;
			}
		}
	}
	else
	{
		if(link.percent[i] < blackThresh)
		{
			isBlack = true;
		}
	}
	return isBlack;
}
//bool isBlackLine_OA(tSensors link, int blackThresh)
//{
//	MSLSA newLink;
//	newLink.port = link;
//	return isBlackLine_OA(newLink, blackThresh, all);
//}

#endif //BLACKDETECT_H
