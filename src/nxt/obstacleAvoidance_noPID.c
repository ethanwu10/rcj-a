#include "Headers\MSLSALineFollow_turn.h"
#include "Headers\obstacleAvoidance_noPID.h"

task main()
{
	SensorType[Arduino] = sensorI2CCustom9V;
	while(true)
	{
		PIDCorrect();
		StartTask(lineFollow);
		Sleep(500);
		{
			ubyte ping_pwr_conf = arduinoPing_getConf(Arduino); //retain sensors enabled by other tasks/programs
			setBit(ping_pwr_conf, SensorFront);
			arduinoPing_setConf(Arduino, ping_pwr_conf);
		}
		Sleep(500);
		while(!(LineFollowState != LineFollowState_Gap && arduinoPing_readInt(Arduino, SensorFront) < tDist))
		{
			Sleep(100); //line tracing
		}
		StopTask(lineFollow);
		motor[LM] = 0;
		motor[RM] = 0;
		obstacleAvoidance();
		motor[LM] = 0;
		motor[RM] = 0;
		Sleep(50);
	}
}
