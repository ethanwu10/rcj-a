#include "Headers/ramp.h"
#include "Headers/move.h"

task main()
{
	SensorType[MPU] = sensorI2CCustom;
	Sleep(500);
	if (!MPU6050_testConnection(MPU))
	{
		PlaySound(soundException);
		nxtDisplayCenteredTextLine(4, "Comm failed");
		Sleep(1000);
		StopAllTasks();
	}
	nSyncedMotors = getSyncMode(motorRight, motorLeft);
	motor[motorRight] = 20;
	while (!isRamp())
	{
		Sleep(100);
	}
	ramp();
	moveStraight(10, 20, tDia, GR, WB);
}
