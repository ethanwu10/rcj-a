#include <Arduino.h>
#include <I2Cdev.h>
#include <MPU6050.h>
#include <Wire.h>

#define START_ON_PROMPT

//#define PRINT_MEASUREMENTS
#define OUTPUT_READABLE_ACCELGYRO
//#define OUTPUT_BINARY_ACCELGYRO

#define SERIALBAUDRATE 115200

#define AVERAGE_SIZE 64
#define ITERATION_CNT 5

MPU6050 MPU;

void printOffs()
{
	Serial.println("ax\tay\taz\tgx\tgy\tgz");
	Serial.print(MPU.getXAccelOffset()); Serial.print("\t");
	Serial.print(MPU.getYAccelOffset()); Serial.print("\t");
	Serial.print(MPU.getZAccelOffset()); Serial.print("\t");
	Serial.print(MPU.getXGyroOffset());  Serial.print("\t");
	Serial.print(MPU.getYGyroOffset());  Serial.print("\t");
	Serial.print(MPU.getZGyroOffset());  Serial.print("\t");
	Serial.println("\n");
}

void setup()
{
	Serial.begin(SERIALBAUDRATE);
	Wire.begin();
	Serial.println("Initializing devices...");
	MPU.initialize();

	Serial.println("Testing device connection...");
	Serial.println(MPU.testConnection() ? "Device connection successful" : "Device connection failed");

	Serial.println("Reading initial sensor offsets...");
	printOffs();

#ifdef START_ON_PROMPT
	Serial.println("Send any character to begin calibration");
	while (Serial.available() && Serial.read()); // empty buffer
    while (!Serial.available());                 // wait for data
    while (Serial.available() && Serial.read()); // empty buffer again
#endif //START_ON_PROMPT

	Serial.println("Reading data to calculate offsets...");
	for (int i = 0; i < ITERATION_CNT; ++i)
	{
		int16_t offs_ax_orig, offs_ay_orig, offs_az_orig, offs_gx_orig, offs_gy_orig, offs_gz_orig;
		int32_t ax_sum, ay_sum, az_sum, gx_sum, gy_sum, gz_sum;
		int16_t ax_avg, ay_avg, az_avg, gx_avg, gy_avg, gz_avg;
		int16_t offs_ax_new, offs_ay_new, offs_az_new, offs_gx_new, offs_gy_new, offs_gz_new;

		ax_sum = ay_sum = az_sum = gx_sum = gy_sum = gz_sum = 0; //clear out sums

		//Read current offsets
		offs_ax_orig = MPU.getXAccelOffset();
		offs_ay_orig = MPU.getYAccelOffset();
		offs_az_orig = MPU.getZAccelOffset();
		offs_gx_orig = MPU.getXGyroOffset();
		offs_gy_orig = MPU.getYGyroOffset();
		offs_gz_orig = MPU.getZGyroOffset();

		//get average
		for (int i = 0; i < AVERAGE_SIZE; ++i)
		{
			int16_t ax, ay, az, gx, gy, gz;
			//read measurements from device
			MPU.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
			ax_sum += ax;
			ay_sum += ay;
			az_sum += az;
			gx_sum += gx;
			gy_sum += gy;
			gz_sum += gz;
			delay(100);
		}
		ax_avg = ax_sum / AVERAGE_SIZE;
		ay_avg = ay_sum / AVERAGE_SIZE;
		az_avg = az_sum / AVERAGE_SIZE;
		gx_avg = gx_sum / AVERAGE_SIZE;
		gy_avg = gy_sum / AVERAGE_SIZE;
		gz_avg = gz_sum / AVERAGE_SIZE;

		//Calculate new offsets
		offs_ax_new = offs_ax_orig - (ax_avg / 8.0);
		offs_ay_new = offs_ay_orig - (ay_avg / 8.0);
		offs_az_new = offs_az_orig - ((az_avg - 16384) / 8.0);
		offs_gx_new = offs_gx_orig - (gx_avg / 4.0);
		offs_gy_new = offs_gy_orig - (gy_avg / 4.0);
		offs_gz_new = offs_gz_orig - (gz_avg / 4.0);

		//Set new offsets
		MPU.setXAccelOffset(offs_ax_new);
		MPU.setYAccelOffset(offs_ay_new);
		MPU.setZAccelOffset(offs_az_new);
		MPU.setXGyroOffset(offs_gx_new);
		MPU.setYGyroOffset(offs_gy_new);
		MPU.setZGyroOffset(offs_gz_new);
	}

	Serial.println("New offsets:");
	printOffs();

	pinMode(LED_BUILTIN, OUTPUT);
}

bool blinkState = false;

void loop()
{
#ifdef PRINT_MEASUREMENTS
	int16_t ax, ay, az, gx, gy, gz;

    // read raw accel/gyro measurements from device
    MPU.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);

    // these methods (and a few others) are also available
    //accelgyro.getAcceleration(&ax, &ay, &az);
    //accelgyro.getRotation(&gx, &gy, &gz);

    #ifdef OUTPUT_READABLE_ACCELGYRO
        // display tab-separated accel/gyro x/y/z values
        Serial.print("a/g:\t");
        Serial.print(ax); Serial.print("\t");
        Serial.print(ay); Serial.print("\t");
        Serial.print(az); Serial.print("\t");
        Serial.print(gx); Serial.print("\t");
        Serial.print(gy); Serial.print("\t");
        Serial.println(gz);
    #endif

    #ifdef OUTPUT_BINARY_ACCELGYRO
        Serial.write((uint8_t)(ax >> 8)); Serial.write((uint8_t)(ax & 0xFF));
        Serial.write((uint8_t)(ay >> 8)); Serial.write((uint8_t)(ay & 0xFF));
        Serial.write((uint8_t)(az >> 8)); Serial.write((uint8_t)(az & 0xFF));
        Serial.write((uint8_t)(gx >> 8)); Serial.write((uint8_t)(gx & 0xFF));
        Serial.write((uint8_t)(gy >> 8)); Serial.write((uint8_t)(gy & 0xFF));
        Serial.write((uint8_t)(gz >> 8)); Serial.write((uint8_t)(gz & 0xFF));
    #endif

    // blink LED to indicate activity
    blinkState = !blinkState;
    digitalWrite(LED_BUILTIN, blinkState);

    delay(50);
#endif //PRINT_MEASUREMENTS
}
