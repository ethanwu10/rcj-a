#pragma once

typedef uint8_t byte;

void setBit(int& num, byte bit)
{
	num |= (1 << bit);
}

void unsetBit(int& num, byte bit)
{
	num &= (~(1 << bit));
}

bool readBit(const int num, byte bit)
{
	return num & (1 << bit);
}
