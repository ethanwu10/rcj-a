#include <Arduino.h>
#include <Wire.h>
#include <NewPing.h>
#include <Timer.h>
#include "bitmap.h"


#define bitsToBytes(x) (x * 8)


#define DEBUG

//#define VERBOSE

#define OUTOFRANGE_255

#define OFF_AS_0

//#define PRINTTOSERIAL


#define I2CAddr 0x05

//Register Map //////////////////////////////////////////////////////////////////////
#define PING_CONF           0x20   //Ping sensor power config
#define PING_MODE           0x21   //Ping sensor reading mode
#define PING_MODE_CFG       0x22   //Ping sensor mode-specific configuration
#define S0_VAL_INTEGER      0x30   //S0 integer distance (cm)
#define S1_VAL_INTEGER      0x31   //S1 integer distance (cm)
#define S2_VAL_INTEGER      0x32   //S2 integer distance (cm)
#define PING_INTEGER_PACKED 0x3f   //Bit pack of all integer distances
#define S0_VAL_DECIMAL      0x40   //S0 2 decimal places of distance (cm)
#define S1_VAL_DECIMAL      0x41   //S1 2 decimal places of distance (cm)
#define S2_VAL_DECIMAL      0x42   //S2 2 decimal places of distance (cm)
#define PING_DECIMAL_PACKED 0x4f   //Bit pack of 2 decimal places of all distances

#define IR_TIMEOUT          0x50   //IR read timeout (in 10ms units)
#define IR_VALUES           0x5f   //IR value

#define ALL_PACKED          0xff   //Bit pack of all values
/////////////////////////////////////////////////////////////////////////////////////

//PING_MODE value map///////////////////////////////////////////////////
#define PING_MODE_MEDIAN 0
#define PING_MODE_FAST   1

#define PING_MODE_DEFAULT PING_MODE_MEDIAN
////////////////////////////////////////////////////////////////////////

//PING_MODE_CFG defaults//////////////Meanings:///////////////////////////
#define PING_MODE_MEDIAN_DEFAULT  5  //median size
#define PING_MODE_FAST_DEFAULT    10 //delay between pings
//////////////////////////////////////////////////////////////////////////

//IR_TIMEOUT default/////////////////////////////////////////
#define IR_TIMEOUT_DEFAULT 50
/////////////////////////////////////////////////////////////


#define S0_TRIGGERPIN 12
#define S0_ECHOPIN 11
#define S1_TRIGGERPIN 10
#define S1_ECHOPIN 9
#define S2_TRIGGERPIN 8
#define S2_ECHOPIN 7

#define MAXDISTANCE 255 //cm


#define S0 0
#define S1 1
#define S2 2


#define VERSON "V1.0    "
#define VENDOR "Arduino "
#define NAME   "  Nano  "


#define SERIALBAUDRATE 9600


typedef float (*getPingVal_Type)(NewPing);

void onI2Crecieve(int);
void onI2Crequest();

float getPingVal_Median(NewPing);
float getPingVal_Fast(NewPing);

void onIRChange();
void onIRTrigger();
void onIRStop();
void resetIRState();

byte readReg = 0x00;


byte ping_conf = 0x07;
byte ping_mode = PING_MODE_DEFAULT;

volatile bool state = false;
volatile bool wasRead = false;
volatile bool hasTurnedOff = false;

Timer reset_timer;
volatile int reset_timer_id;
unsigned int reset_timeout = IR_TIMEOUT_DEFAULT;

float S_dists[3] = {0,0,0};
byte S_dists_integer[3] = {0,0,0};
byte S_dists_decimal[3] = {0,0,0};

NewPing sonar[3] =
{
	NewPing(S0_TRIGGERPIN, S0_ECHOPIN, MAXDISTANCE),
	NewPing(S1_TRIGGERPIN, S1_ECHOPIN, MAXDISTANCE),
	NewPing(S2_TRIGGERPIN, S2_ECHOPIN, MAXDISTANCE)
};

getPingVal_Type getPingVal[] =
{
	getPingVal_Median,
	getPingVal_Fast
};

byte ping_mode_cfg[] =
{
	PING_MODE_MEDIAN_DEFAULT,
	PING_MODE_FAST_DEFAULT
};

void setup()
{
	Serial.begin(SERIALBAUDRATE);
	pinMode(3, INPUT_PULLUP);
	attachInterrupt(1, onIRChange, CHANGE);
	Wire.begin(I2CAddr);
	Wire.onReceive(onI2Crecieve);
	Wire.onRequest(onI2Crequest);
	pinMode(13, OUTPUT);
}

void loop()
{
//
//#ifdef DEBUG
//	digitalWrite(13, HIGH);
//#endif // DEBUG
//
	for(int i = 0; i < (sizeof(S_dists) / sizeof(*S_dists)); i++)
	{
		if (readBit(ping_conf, i))
		{
			S_dists[i] = getPingVal[ping_mode](sonar[i]);	  //Calculate distance as floating-point
		}
	}

#ifdef OUTOFRANGE_255
	for(int i = 0; i < (sizeof(S_dists) / sizeof(*S_dists)); i++)
	{
		if (S_dists[i] == 0)
		{
			S_dists[i] = 255;
		}
	}
#endif // OUTOFRANGE_255

#ifdef OFF_AS_0
	for(int i = 0; i < (sizeof(S_dists) / sizeof(*S_dists)); i++)
	{
		if (!readBit(ping_conf, i))
		{
			S_dists[i] = 0;
		}
	}
#endif // OFF_AS_0

	for(int i = 0; i < (sizeof(S_dists) / sizeof(*S_dists)); i++)
	{
		S_dists_integer[i] = floor(S_dists[i]);                               //convert to integer value
		S_dists_decimal[i] = round((S_dists[i] - S_dists_integer[i]) * 100);  //Get 2 decimal places (as an integer)
	}

#ifdef PRINTTOSERIAL
	for(int i = 0; i < (sizeof(S_dists) / sizeof(*S_dists)); i++)
	{
		Serial.println(S_dists[i]);
	}
	Serial.println();
#endif // PRINTTOSERIAL


	if (digitalRead(3) == LOW) //Update IR/silver line states
	{
		state = true;
		hasTurnedOff = false;
	}

	reset_timer.update();

//
//#ifdef DEBUG
//	digitalWrite(13, LOW);
//#endif // DEBUG
//
	delayMicroseconds(100);
}


float getPingVal_Median(NewPing ping)
{
	return ping.ping_median(ping_mode_cfg[PING_MODE_MEDIAN]) / float(US_ROUNDTRIP_CM);
}

float getPingVal_Fast(NewPing ping)
{
	delay(ping_mode_cfg[PING_MODE_FAST]);
	return ping.ping() / float(US_ROUNDTRIP_CM);
}

//////////////////////////////////////////////////////////////////////////////////////////////////

bool readIR()
{
	bool val = state;
	wasRead = true;
	if (state && hasTurnedOff)
	{
		hasTurnedOff = false;
		state = false;
		reset_timer.stop(reset_timer_id);
	}
	return val;
}

void onIRChange()
{
	if ((bool)(PIND & _BV(PIND3)) == LOW)
	{
		onIRTrigger();
	}
	else
	{
		onIRStop();
	}
}

void onIRTrigger()
{
	if ((bool)(PIND & _BV(PIND3)) != LOW)
	{
		onIRStop();
		return;
	}
	state = true;
	wasRead = false;
	hasTurnedOff = false;
	reset_timer.stop(reset_timer_id);
}

void onIRStop()
{
	if (!wasRead)
	{
		hasTurnedOff = true;
		reset_timer_id = reset_timer.after((reset_timeout * 10), resetIRState);
	}
	else
	{
		state = false;
	}
}

void resetIRState()
{
	hasTurnedOff = false;
	state = false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////

void onI2Crecieve(int length)
{
#ifdef DEBUG

	digitalWrite(13, HIGH);

#endif // DEBUG

	noInterrupts();

	if (length != 1)
	{
		readReg = Wire.read();
#ifdef VERBOSE
		Serial.println("Message received:");
		Serial.print(length);
		Serial.println(" bytes");
		Serial.print("Register: ");
		Serial.println(readReg);
#endif // VERBOSE

		while(Wire.available() > 0)
		{
			byte val = Wire.read();
			switch(readReg)
			{
			case PING_CONF:
				ping_conf = val;
				break;
			case PING_MODE:
				ping_mode = val;
				break;
			case PING_MODE_CFG:
				ping_mode_cfg[ping_mode] = val;
				break;
			case IR_TIMEOUT:
				reset_timeout = val;
			default:
				;
			}
			Serial.println(val, BIN);
		}
		Serial.println('\n');
	}
	else
	{
		readReg = Wire.read();

#ifdef VERBOSE
		Serial.println('\n');
#endif // VERBOSE
	}

	interrupts();

#ifdef DEBUG

	digitalWrite(13, LOW);

#endif // DEBUG

}

void onI2Crequest(void)
{
#ifdef DEBUG

	digitalWrite(13, HIGH);

#endif // DEBUG

	//noInterrupts();

	switch(readReg)
	{
	case 0x00: //Version
		Wire.write(VERSON);
		break;
	case 0x08: //Vendor
		Wire.write(VENDOR);
		break;
	case 0x10: //Device name
		Wire.write(NAME);
		break;
	case PING_CONF:
		Wire.write(ping_conf);
		break;
	case PING_MODE:
		Wire.write(ping_mode);
		break;
	case PING_MODE_CFG:
		Wire.write(ping_mode_cfg[ping_mode]);
		break;
	case S0_VAL_INTEGER:
		Wire.write(S_dists_integer[0]);
		break;
	case S1_VAL_INTEGER:
		Wire.write(S_dists_integer[1]);
		break;
	case S2_VAL_INTEGER:
		Wire.write(S_dists_integer[2]);
		break;
	case PING_INTEGER_PACKED:
		Wire.write(S_dists_integer, 3);
		break;
	case S0_VAL_DECIMAL:
		Wire.write(S_dists_decimal[0]);
		break;
	case S1_VAL_DECIMAL:
		Wire.write(S_dists_decimal[1]);
		break;
	case S2_VAL_DECIMAL:
		Wire.write(S_dists_decimal[2]);
		break;
	case PING_DECIMAL_PACKED:
		Wire.write(S_dists_decimal, 3);
		break;
	case IR_TIMEOUT:
		Wire.write(reset_timeout);
		break;
	case IR_VALUES:
		Wire.write(readIR() ? 0xff : 0x0);
		break;

	case ALL_PACKED:
		{
			byte msg[7];
			msg[0] = S_dists_integer[0];
			msg[1] = S_dists_integer[1];
			msg[2] = S_dists_integer[2];
			msg[3] = S_dists_decimal[0];
			msg[4] = S_dists_decimal[1];
			msg[5] = S_dists_decimal[2];
			msg[6] = readIR();
			Wire.write(msg, 7);
		}
		break;

	default:
		Wire.write("0");
	}

	//interrupts();

#ifdef DEBUG

	digitalWrite(13, LOW);

#endif // DEBUG
}
